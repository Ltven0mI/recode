#include "Map.h"
#include "Camera.h"
#include "Chunk.h"
#include "Graphics.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <cmath>


uint8_t Map::_width { 0 };
uint8_t Map::_height { 0 };
uint8_t Map::_chunkWidth { 0 };
uint8_t Map::_chunkHeight { 0 };
int Map::_tileSize { 0 };

std::string Map::_saveName;

Tile* Map::_activeTile { NULL };

std::vector< std::vector<Chunk*> > Map::chunkGrid { };


void Map::init(std::string saveName, int tileSize)
{
	_tileSize = tileSize;
	openSave(saveName);
}

void Map::init(std::string saveName, uint8_t width, uint8_t height, uint8_t chunkWidth, uint8_t chunkHeight, int tileSize)
{
	_width = width;
	_height = height;
	_chunkWidth = chunkWidth;
	_chunkHeight = chunkHeight;
	_tileSize = tileSize;

	initChunks();
	saveMap(saveName);
}

void Map::cleanup()
{

}


void Map::update(double dt)
{
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _height; y++)
		{
			chunkGrid[x][y]->update(dt);
		}
	}
}

void Map::draw()
{
	int cameraX = 0;
	int cameraY = 0;
	Camera::getPosition(&cameraX, &cameraY);

	int chunkPixWidth = _chunkWidth * _tileSize;
	int chunkPixHeight = _chunkHeight * _tileSize;
	for (int x = 0; x < _width; x++)
	{
		for (int y = 0; y < _height; y++)
		{
			Chunk* chunk = chunkGrid[x][y];
			chunk->draw(x * chunkPixWidth - cameraX, y * chunkPixHeight - cameraY, _tileSize);
		}
	}
}


int Map::getWidth()
{
	return _width;
}

int Map::getHeight()
{
	return _height;
}

int Map::getChunkWidth()
{
	return _chunkWidth;
}

int Map::getChunkHeight()
{
	return _chunkHeight;
}


Chunk* Map::getChunk(int x, int y)
{
	return chunkGrid[x][y];
}


Tile* Map::getActiveTile()
{
	return _activeTile;
}

void Map::setActiveTile(Tile* tile)
{
	if (_activeTile != NULL)
		_activeTile->isActve = false;

	Terminal::setTerminal(NULL);

	_activeTile = tile;
	if (_activeTile != NULL)
	{
		_activeTile->isActve = true;
		Terminal::setTerminal(_activeTile->getTerminal());
	}
}


Tile* Map::getTileAtWorldPosition(int worldX, int worldY)
{
	if (worldX >= 0 && worldX < _width * _chunkWidth * _tileSize && worldY >= 0 && worldY < _height * _chunkHeight * _tileSize)
	{
		int chunkX, chunkY;
		worldToChunkSpace(worldX, worldY, &chunkX, &chunkY);
		Chunk* chunk = getChunk(chunkX, chunkY);
		int chunkWorldX = chunkX * _chunkWidth * _tileSize;
		int chunkWorldY = chunkY * _chunkHeight * _tileSize;
		int tileX = (int)std::floor((double)(worldX - chunkWorldX) / _tileSize);
		int tileY = (int)std::floor((double)(worldY - chunkWorldY) / _tileSize);
		return chunk->getTile(tileX, tileY);
	}
	return NULL;
}

bool Map::addTileAtWorldPosition(int worldX, int worldY, Tile* tile)
{
	if (worldX >= 0 && worldX < _width * _chunkWidth * _tileSize && worldY >= 0 && worldY < _height * _chunkHeight * _tileSize)
	{
		int chunkX, chunkY;
		worldToChunkSpace(worldX, worldY, &chunkX, &chunkY);
		Chunk* chunk = getChunk(chunkX, chunkY);
		int chunkWorldX = chunkX * _chunkWidth * _tileSize;
		int chunkWorldY = chunkY * _chunkHeight * _tileSize;
		int tileX = (int)std::floor((double)(worldX - chunkWorldX) / _tileSize);
		int tileY = (int)std::floor((double)(worldY - chunkWorldY) / _tileSize);
		createTileSave(tile);
		return chunk->addTile(tileX, tileY, tile);
	}
	return false;
}

void Map::removeTileAtWorldPosition(int worldX, int worldY)
{
	if (worldX >= 0 && worldX < _width * _chunkWidth * _tileSize && worldY >= 0 && worldY < _height * _chunkHeight * _tileSize)
	{
		int chunkX, chunkY;
		worldToChunkSpace(worldX, worldY, &chunkX, &chunkY);
		Chunk* chunk = getChunk(chunkX, chunkY);
		int chunkWorldX = chunkX * _chunkWidth * _tileSize;
		int chunkWorldY = chunkY * _chunkHeight * _tileSize;
		int tileX = (int)std::floor((double)(worldX - chunkWorldX) / _tileSize);
		int tileY = (int)std::floor((double)(worldY - chunkWorldY) / _tileSize);
		chunk->removeTileAtImmediate(tileX, tileY);
	}
}


void Map::worldToChunkSpace(int worldX, int worldY, int* chunkX, int* chunkY)
{
	*chunkX = (int)std::floor((double)worldX / _chunkWidth / _tileSize);
	*chunkY = (int)std::floor((double)worldY / _chunkHeight / _tileSize);
}

void Map::worldToTileSpace(int worldX, int worldY, int* tileX, int* tileY)
{
	*tileX = (int)std::floor((double)worldX / _tileSize);
	*tileY = (int)std::floor((double)worldY / _tileSize);
}


void Map::initChunks()
{
	chunkGrid.resize(_width);
	for (int x = 0; x < _width; x++)
	{
		chunkGrid[x].resize(_height);
		for (int y = 0; y < _height; y++)
		{
			chunkGrid[x][y] = new Chunk(x, y, _chunkWidth, _chunkHeight);
		}
	}
}


static inline bool isnotvalidchar(char c)
{
	return !(isalpha(c) || isdigit(c) || (c == ' ') || (c == '-') || (c== '_'));
}

bool Map::validateSaveName(std::string saveName)
{
	return find_if(saveName.begin(), saveName.end(), isnotvalidchar) == saveName.end();
}


std::string Map::getSavePath()
{
	return DIR_DATA + "saves/" + _saveName + "/";
}

std::string Map::getFullSavePath()
{
	return SDL_GetBasePath() + DIR_DATA + "saves/" + _saveName + "/";
}

bool Map::openSave(std::string saveName)
{
	_saveName = saveName;
	std::string savePath = getFullSavePath();
	if (FileUtil::isDirectory(savePath))
	{
		std::string serializedData;
		if (FileUtil::openFile(savePath + "/.map", serializedData) == true)
		{
			deserialize(serializedData);
			return true;
		}
		std::cout << "[Map] Failed to open save data at: '" << DIR_DATA + "saves/.map'" << std::endl;
		return false;
	}
	std::cout << "[Map] Failed to open save: " << saveName << "! At directory: " << DIR_DATA << "saves/" << saveName << std::endl;
	return false;
}

bool Map::saveMap(std::string saveName)
{
	_saveName = saveName;
	std::string dirPath = getFullSavePath();

	if (FileUtil::isDirectory(dirPath) == false)
	{
		if (FileUtil::createDirectory(dirPath) == false)
		{
			std::cout << "[Map] Failed to create save directory: '" << dirPath << "'" << std::endl;
			return false;
		}
	}

	std::string tilesPath = dirPath + "tiles/";
	if (FileUtil::isDirectory(tilesPath) == false)
	{
		if (FileUtil::createDirectory(tilesPath) == false)
		{
			std::cout << "[Map] Failed to create save tile directory: '" << tilesPath << "'" << std::endl;
			return false;
		}
	}

	std::string mapDataPath = dirPath + "/.map";
	std::ofstream file(mapDataPath);
	if (file.is_open() == false)
	{
		std::cout << "[Map] Failed to save map to: '" << mapDataPath << "'" << std::endl;
		return false;
	}
	std::string text = serialize();
	file << text;
	file.close();
	return true;
}


bool Map::createTileSave(Tile* tile)
{
	std::string tilePath = getFullSavePath() + "tiles/" + std::to_string(tile->getID()) + "/";
	if (FileUtil::isDirectory(tilePath) == false)
	{
		if (FileUtil::createDirectory(tilePath) == false)
		{
			std::cout << "[Map] Failed to create tile save directory: '" << tilePath << "'" << std::endl;
			return false;
		}
		return true;
	}
	return false;
}


std::string Map::serialize()
{
	std::string result = "";
	result.push_back(_width);
	result.push_back(_height);
	result.push_back(_chunkWidth);
	result.push_back(_chunkHeight);

	for (uint8_t cx = 0; cx < _width; cx++)
	{
		for (uint8_t cy = 0; cy < _height; cy++)
		{
			Chunk* chunk = chunkGrid[cx][cy];
			std::list<Tile*>::const_iterator iterator, end;
			for (iterator = chunk->tileList.begin(), end = chunk->tileList.end(); iterator != end; ++iterator)
			{
				Tile* tile = *iterator;
				uint8_t idRight = tile->getID() & 0x00ff;
				uint8_t idLeft = tile->getID() >> 8;
				result.push_back(idLeft);
				result.push_back(idRight);
				result.push_back(tile->x);
				result.push_back(tile->y);
				result.push_back(cx);
				result.push_back(cy);
			}
		}
	}

	return result;
}

void Map::deserialize(std::string serializedMap)
{
	_width = serializedMap[0];
	_height = serializedMap[1];
	_chunkWidth = serializedMap[2];
	_chunkHeight = serializedMap[3];

	initChunks();

	size_t pos = 4;
	if (serializedMap.length() - pos > 0 && (serializedMap.length() - pos) % 6 == 0)
	{
		while (pos < serializedMap.length())
		{
			uint8_t idL = serializedMap[pos + 0];
			uint8_t idR = serializedMap[pos + 1];
			uint16_t id = (idL << 8) | idR;

			uint8_t tx = serializedMap[pos + 2];
			uint8_t ty = serializedMap[pos + 3];
			uint8_t cx = serializedMap[pos + 4];
			uint8_t cy = serializedMap[pos + 5];

			chunkGrid[cx][cy]->addTile(tx, ty, new Tile(id));

			pos += 6;
		}
	}
}
