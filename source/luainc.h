#ifndef __LUA_INC_H__
#define __LUA_INC_H__

extern "C"
{
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}

#endif // __LUA_INC_H__
