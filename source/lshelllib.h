#include "Interpreter.h"
#include "Tile.h"
#include "Action.h"
#include "luainc.h"

#include <SDL2/SDL.h>


int shell_sleep(lua_State* L)
{
	if (lua_isnumber(L, -1) == 1)
	{
		uint32_t sleepTime = lua_tonumber(L, -1);
		SDL_Delay(sleepTime);
		return 0;
	}
	std::string errorMsg = "Attempt to call 'shell.sleep(number)' with non numeric argument!";
	lua_pushlstring(L, errorMsg.c_str(), errorMsg.length());
	lua_error(L);
	return 0;
}


static const luaL_Reg shelllib[] = {
	{ "sleep", shell_sleep },
	{ NULL, NULL }
};


LUAMOD_API int luaopen_shell(lua_State *L)
{
	luaL_newlib(L, shelllib);
	lua_setglobal(L, "shell");
	return 1;
}