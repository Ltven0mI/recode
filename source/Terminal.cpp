#include "Terminal.h"
#include "TextEditor.h"
#include "FileUtil.h"
#include "StringUtil.h"
#include "PathUtil.h"
#include "Graphics.h"
#include "Keyboard.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iterator>

Terminal::Terminal(Tile* tile, std::string user, std::string globalPath, std::string homePath)
{
	_isActiveTerminal = false;
	_isReading = false;
	_isRoot = false;

	_user = user;

	_tile = tile;

	_globalPath = globalPath;
	_homePath = homePath;
	_relativePath = "";

	_lines = std::vector<std::string>();
	_lineSurfaces = std::vector<SDL_Surface*>();
	_lineTextures = std::vector<SDL_Texture*>();
	_input = "";

	_actionMutex = SDL_CreateMutex();
	_actions = std::list<Action*>();
	_currentAction = NULL;

	_scroll = 0;

	startNewCommandLine();
}

Terminal::Terminal(std::string user, std::string globalPath, std::string homePath)
{
	_isActiveTerminal = false;
	_isReading = false;
	_isRoot = false;

	_user = user;

	_tile = NULL;

	_globalPath = globalPath;
	_homePath = homePath;
	_relativePath = "";

	_lines = std::vector<std::string>();
	_lineSurfaces = std::vector<SDL_Surface*>();
	_lineTextures = std::vector<SDL_Texture*>();
	_input = "";

	_actionMutex = SDL_CreateMutex();
	_actions = std::list<Action*>();
	_currentAction = NULL;

	_scroll = 0;

	startNewCommandLine();
}

Terminal::~Terminal()
{
	_imediateClear();
	SDL_DestroyMutex(_actionMutex);
}


// Updating //
void Terminal::update(double dt)
{
	SDL_LockMutex(_actionMutex);	// Lock Action Mutex
	if (_actions.size() > 0 && _currentAction == NULL)
		_doAction(_actions.front());
	SDL_UnlockMutex(_actionMutex);	// Unlock Action Mutex
}


// Instance Getters //
bool Terminal::getIsRoot()
{
	return _isRoot;
}

std::string Terminal::getUser()
{
    return _user;
}

std::string Terminal::getGlobalPath()
{
    return _globalPath;
}

std::string Terminal::getHomePath()
{
    return _homePath;
}

std::string Terminal::getRelativePath()
{
    return _relativePath;
}

void Terminal::setRelativePath(std::string relativePath)
{
    _relativePath = relativePath;
}

int Terminal::_getLineCount()
{
	return (int)_lines.size();
}


// Line Util //
void Terminal::addLine(std::string line)
{
	if (_getLineCount() > static_maxLineCount)
		_removeLastLine();

	_lines.push_back(line);
	_lineSurfaces.push_back(TTF_RenderText_Blended(static_font, line.c_str(), static_fontColour));
	_lineTextures.push_back(NULL);
}

void Terminal::_addMultiline(std::string text)
{
	std::stringstream stream(text);
	std::string line;
	bool first = true;
	while (std::getline(stream, line, '\n'))
	{
		if (first)
		{
			_appendToLine(_getLineCount() - 1, line);
			first = false;
		}
		else
		{
			addLine(line);
		}
	}
	if (text.back() == '\n')
	{
		if (_getLineCount() > static_maxLineCount)
			_removeLastLine();

		_lines.push_back("");
		_lineSurfaces.push_back(NULL);
		_lineTextures.push_back(NULL);
	}
}

void Terminal::_appendToLine(int index, std::string text)
{
	if (_lineSurfaces[index] != NULL)
		SDL_FreeSurface(_lineSurfaces[index]);
	if (_lineTextures[index] != NULL)
		SDL_DestroyTexture(_lineTextures[index]);

	_lines[index].append(text);
	_lineSurfaces[index] = TTF_RenderText_Blended(static_font, _lines[index].c_str(), static_fontColour);
	_lineTextures[index] = NULL;
}

void Terminal::startNewCommandLine()
{
	if (_getLineCount() > static_maxLineCount)
		_removeLastLine();

	std::string line = _homePath + _relativePath + "> ";
	_lines.push_back(line);
	_lineSurfaces.push_back(TTF_RenderText_Blended(static_font, line.c_str(), static_fontColour));
	_lineTextures.push_back(NULL);
}

void Terminal::_removeLastLine()
{
	if (_lineSurfaces.front() != NULL)
		SDL_FreeSurface(_lineSurfaces.front());
	if (_lineTextures.front() != NULL)
		SDL_DestroyTexture(_lineTextures.front());

	_lines.erase(_lines.begin());
	_lineSurfaces.erase(_lineSurfaces.begin());
	_lineTextures.erase(_lineTextures.begin());
}


// Action Handlers //
void Terminal::_addAction(Action* action)
{
	SDL_LockMutex(_actionMutex);	// Lock Action Mutex
	_actions.push_back(action);
	SDL_UnlockMutex(_actionMutex);	// Unlock Action Mutex
}

void Terminal::_doAction(Action* action)
{
	_currentAction = action;
	switch (action->getActionType())
	{
	case TERM_ACTION_READ:
		if (_startReading() == false)
			_onCompletedAction("");
		break;
	case TERM_ACTION_WRITE:
		_doWrite();
		break;
	case TERM_ACTION_CLEAR:
		_doClear();
		break;
	case TERM_ACTION_PROGRAMEND:
		_doProgramEnded();
		break;
	default:
		std::cout << "[Terminal] Attempted to do an unsupported action of type: " << (int)action->getActionType() << std::endl;
		_onCompletedAction("");
		break;
	}
}

void Terminal::_onCompletedAction(std::string result)
{
	SDL_LockMutex(_actionMutex);	// Lock Action Mutex
	Action* action = _actions.front();
	action->result_string = result;
	action->isCompleted = true;
	_actions.pop_front();
	_currentAction = NULL;
	SDL_UnlockMutex(_actionMutex);	// Unlock Action Mutex
}

void Terminal::_clearActions()
{
	if (_isReading == true)
		addLine("");
	_isReading = false;
	if (_actions.empty() == false)
	{
		_onCompletedAction("");
		SDL_LockMutex(_actionMutex);	// Lock Action Mutex
		_actions.clear();
		SDL_UnlockMutex(_actionMutex);	// Unlock Action Mutex
	}
}


// Action Methods //
Action* Terminal::read()
{
	Action* newAction = new Action(TERM_ACTION_READ);
	_addAction(newAction);
	return newAction;
}

Action* Terminal::write(std::string text)
{
	Action* newAction = new Action(TERM_ACTION_WRITE);
	newAction->arg_string = text;
	_addAction(newAction);
	return newAction;
}

Action* Terminal::clear()
{
	Action* newAction = new Action(TERM_ACTION_CLEAR);
	_addAction(newAction);
	return newAction;
}

Action* Terminal::programEnded()
{
	Action* newAction = new Action(TERM_ACTION_PROGRAMEND);
	_addAction(newAction);
	return newAction;
}


// Action Starters //
bool Terminal::_startReading()
{
	_isReading = true;
	return true;
}

void Terminal::_onReadingFinished()
{
	_isReading = false;
	if (_getLineCount() >= static_lineFitCount)
		_scrollTo(_getLineCount() - 1);
	_onCompletedAction(_input);
}

void Terminal::_doWrite()
{
	_addMultiline(_currentAction->arg_string);
	if (_getLineCount() >= static_lineFitCount)
		_scrollTo(_getLineCount() - 1);
	_onCompletedAction("");
}

void Terminal::_doClear()
{
	_imediateClear();
	_onCompletedAction("");
}

void Terminal::_doProgramEnded()
{
	_appendToLine(_getLineCount() - 1, _homePath + _relativePath + "> ");
	_onCompletedAction("");
}


void Terminal::_imediateClear()
{
	_scroll = 0;

	for (int i = 0; i < _getLineCount(); i++)
	{
		if (_lineSurfaces[i] != NULL)
			SDL_FreeSurface(_lineSurfaces[i]);
		if (_lineTextures[i] != NULL)
			SDL_DestroyTexture(_lineTextures[i]);
	}

	_lines.clear();
	_lineSurfaces.clear();
	_lineTextures.clear();

	_lines.push_back("");
	_lineSurfaces.push_back(NULL);
	_lineTextures.push_back(NULL);
}


// Scrollers //
bool Terminal::_scrollUp()
{
	if (_scroll > 0)
	{
		_scroll--;
		return true;
	}
	return false;
}

bool Terminal::_scrollDown()
{
	if (_scroll < _getLineCount() - 1)
	{
		_scroll++;
		return true;
	}
	return false;
}

void Terminal::_scrollTo(int scroll)
{
	if (scroll < _scroll)
		_scroll = scroll;
	else
		_scroll = scroll - static_lineFitCount + 1;
}


// Line Rendering //
void Terminal::_redrawLineAt(int index)
{
	SDL_FreeSurface(_lineSurfaces[index]);
	SDL_DestroyTexture(_lineTextures[index]);

	std::string lineText = _lines[index];
	if (index == _getLineCount() - 1)	// Append Terminal Input
		lineText.append(_input);

	_lineSurfaces[index] = TTF_RenderText_Blended(static_font, lineText.c_str(), static_fontColour);
	_lineTextures[index] = NULL;
}

void Terminal::_redrawLineRange(int start, int count)
{
	for (int i = start; i < start + count; i++)
	{
		SDL_FreeSurface(_lineSurfaces[i]);
		SDL_DestroyTexture(_lineTextures[i]);

		std::string lineText = _lines[i];
		if (i == _getLineCount() - 1)	// Append Terminal Input
			lineText.append(_input);

		_lineSurfaces[i] = TTF_RenderText_Blended(static_font, lineText.c_str(), static_fontColour);
		_lineTextures[i] = NULL;
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////
// STATIC //////////
////////////

std::string	Terminal::static_basePath;
bool		Terminal::static_isActive;

int			Terminal::static_lineFitCount;
int			Terminal::static_maxLineCount;

int			Terminal::static_padding;
int			Terminal::static_fontSize;
int			Terminal::static_fontHeight;
Terminal*	Terminal::static_activeTerminal;
SDL_Rect*	Terminal::static_windowRect;
SDL_Color	Terminal::static_fontColour;
TTF_Font*	Terminal::static_font;

Terminal*		Terminal::static_rootTerminal;
Interpreter*	Terminal::static_rootInterpreter;


void Terminal::init(int w, int h, int maxLineCount, int padding, int fontSize)
{
    Commands::init();

	static_basePath = SDL_GetBasePath();
	static_isActive = false;

	static_maxLineCount = maxLineCount;
	static_padding = padding;
	static_fontSize = fontSize;

	static_fontHeight = 0;
	static_activeTerminal = NULL;
	static_fontColour = SDL_Color { 0x00, 0x8f, 0xf0, 0x00 };
	static_font = openFont(DIR_DATA + "fonts/SourceCodePro-Regular.ttf");
	if (static_font == NULL)
	{
		std::cout << "[Terminal] Failed to open font: " << static_basePath + DIR_DATA + "fonts/SourceCodePro-Regular.ttf" << std::endl;
		exit(1);
	}
	TTF_SizeText(static_font, " ", NULL, &static_fontHeight);

	static_lineFitCount = (int)std::floor((double)(h - static_padding * 2) / static_fontHeight);
	static_windowRect = new SDL_Rect { padding, padding, w - padding * 2, static_lineFitCount * static_fontHeight };

	static_rootTerminal = new Terminal("root", "programs/", "root/");

	static_rootInterpreter = new Interpreter(static_rootTerminal);
	static_rootInterpreter->startThread();
}

void Terminal::cleanup()
{
	delete static_rootInterpreter;
	delete static_rootTerminal;
	delete static_windowRect;
	TTF_CloseFont(static_font);
}


// Getters and Setters //
void Terminal::setTerminal(Terminal* terminal)
{
	if (static_activeTerminal != NULL)
		static_activeTerminal->_isActiveTerminal = false;

	static_activeTerminal = terminal;

	if (static_activeTerminal != NULL)
		static_activeTerminal->_isActiveTerminal = true;
}

bool Terminal::getIsActive()
{
	return static_isActive;
}

void Terminal::setActive(bool active)
{
	static_isActive = active;
}

Terminal* Terminal::getRootTerminal()
{
	return static_rootTerminal;
}


// Drawing //
void Terminal::draw()
{
	if (static_isActive == true)
	{
		Graphics::setColor(10, 10, 10, 255);
		Graphics::fillRect(static_windowRect);
		Graphics::setClipRect(static_windowRect);

		Terminal* term = getTerminalInUse();

		if (term != NULL)
		{
			SDL_Rect* lineRect = new SDL_Rect { static_windowRect->x, static_windowRect->y, 0, 0 };
			int startIndex = term->_scroll;
			int endIndex = (int)std::fmin(term->_lines.size(), startIndex + static_lineFitCount);
			for (int i = startIndex; i < endIndex; i++)
			{
				SDL_Surface* lineSurface = term->_lineSurfaces[i];
				if (lineSurface != NULL)
				{
					if (term->_lineTextures[i] == NULL)
						term->_lineTextures[i] = SDL_CreateTextureFromSurface(Graphics::getRenderer(), lineSurface);

					lineRect->w = lineSurface->w;
					lineRect->h = static_fontHeight;

					int result = Graphics::drawTexture(term->_lineTextures[i], &lineSurface->clip_rect, lineRect);
					if (result != 0)
					{
						std::cout << "Failed to Copy line: " << i << " to renderer! " << SDL_GetError() << std::endl;
					}
				}
				lineRect->y += static_fontHeight;
			}
			delete lineRect;
		}

		Graphics::setClipRect(NULL);
	}
}


// Utility Methods //
bool Terminal::doTakeInput()
{
	return (static_isActive && (static_activeTerminal != NULL || static_rootTerminal != NULL));
}

Terminal* Terminal::getTerminalInUse()
{
	if (static_activeTerminal != NULL)
		return static_activeTerminal;
	return static_rootTerminal;
}

Interpreter* Terminal::getInterpreterInUse()
{
	if (static_activeTerminal != NULL)
		return static_activeTerminal->_tile->getInterpreter();
	return static_rootInterpreter;
}


// Openers //
TTF_Font* Terminal::openFont(std::string path)
{
	std::string fullPath = static_basePath + path;
	return TTF_OpenFont(fullPath.c_str(), static_fontSize);
}

bool Terminal::openFile(std::string path, std::string &contents)
{
	std::ifstream stream(static_basePath + path, std::ios::in | std::ios::binary);
	if (stream.is_open() == true)
	{
		stream.seekg(0, std::ios::end);
		contents.resize((size_t)stream.tellg());
		stream.seekg(0, std::ios::beg);
		stream.read(&contents[0], contents.size());
		stream.close();
		return true;
	}
	return false;
}


std::vector<std::string> extractArgs(std::string line)
{
	std::vector<std::string> args;
	bool stringOpen = false;
	int argStart = -1;
	for (size_t i = 0; i < line.length(); i++)
	{
		char c = line[i];
		if (c == '\"')
		{
			if (stringOpen == false && argStart != -1)
			{
				args.push_back(line.substr(argStart, i - argStart));
				argStart = -1;
			}

			if (stringOpen == false && argStart == -1)
			{
				stringOpen = true;
				argStart = i + 1;
			}
			else if (stringOpen == true)
			{
				args.push_back(line.substr(argStart, i - argStart));

				stringOpen = false;
				argStart = -1;
			}
		}
		else if (c == ' ' && argStart != -1 && stringOpen == false)
		{
			args.push_back(line.substr(argStart, i - argStart));
			argStart = -1;
		}
		else if (c != ' ' && argStart == -1 && stringOpen == false)
		{
			argStart = i;
		}
	}

	if (argStart != -1)
	{
		args.push_back(line.substr(argStart, -1));
	}
	return args;
}

bool Terminal::parseCommand(std::string line, Terminal* term, Interpreter* interpreter)
{
	if (line.empty() == false)
	{
		std::vector<std::string> args = extractArgs(line);

		std::string command = args[0];
		args.erase(args.begin());

		Command* targetCommand = Commands::getCommand(command);
		if (targetCommand != NULL)
		{
            int err = targetCommand->run(args, term, interpreter);
            switch (err)
            {
            case ERROR_COMMAND_FAILED:
                term->addLine("Command failed");
                break;
            case ERROR_COMMAND_MISSINGARGS:
                term->addLine("Missing required args");
                break;
            }
		}
		else
		{
			std::string relativePath = term->_relativePath;
			if (command.front() == '/')
				relativePath = "";
			std::string validatedPath = PathUtil::validatePath(relativePath + command);
			std::string validatedCommandPath = PathUtil::validatePath(command);

            std::string program;
			if (openFile(Map::getSavePath() + term->_globalPath + validatedPath, program) == true)
			{
				interpreter->runProgram(program, args);
				term->addLine("");
				return true;
			}
			else if (openFile(Map::getSavePath() + "programs/" + validatedCommandPath + ".lua", program) == true)
			{
				interpreter->runProgram(program, args);
				term->addLine("");
				return true;
			}
			else if (openFile(Map::getSavePath() + "programs/" + validatedCommandPath, program) == true)
			{
				interpreter->runProgram(program, args);
				term->addLine("");
				return true;
			}
			else if (openFile(DIR_DATA + "lua/programs/" + validatedCommandPath + ".lua", program) == true)
			{
				interpreter->runProgram(program, args);
				term->addLine("");
				return true;
			}
			else
			{
				term->addLine("No such program '" + command + "'");
			}
		}
	}
	term->startNewCommandLine();
	return false;
}


// Callback Handlers //
void Terminal::onKeyPressed(SDL_Keycode keycode, uint16_t mod)
{
	if (doTakeInput())
	{
		Terminal* term = getTerminalInUse();
		Interpreter* interpreter = getInterpreterInUse();
		switch (keycode)
		{
		case SDLK_t:
			if (mod & KMOD_CTRL && interpreter->getIsProgramRunning())
			{
				interpreter->terminateProgram();
				term->_clearActions();
				term->_input = "";
			}
			break;
		case SDLK_PAGEUP:
			term->_scrollUp();
			break;
		case SDLK_PAGEDOWN:
			term->_scrollDown();
			break;
		case SDLK_RETURN:
			if (interpreter->getIsProgramRunning())	// Inside a program
			{
				if (term->_isReading == true)
				{
					term->_appendToLine(term->_getLineCount() - 1, term->_input);
					term->addLine("");
					term->_onReadingFinished();
					term->_input = "";

					if (term->_getLineCount() >= static_lineFitCount)
						term->_scrollTo(term->_getLineCount() - 1);
				}
			}
			else	// Inside terminal
			{
				term->_appendToLine(term->_getLineCount() - 1, term->_input);
				std::string command = term->_input;
				term->_input = "";
				parseCommand(command, term, interpreter);

				if (term->_getLineCount() >= static_lineFitCount)
					term->_scrollTo(term->_getLineCount() - 1);
			}
			break;
		case SDLK_BACKSPACE:
			if (interpreter->getIsProgramRunning() && term->_isReading || interpreter->getIsProgramRunning() == false)
			{
				std::string curString = term->_input;
				std::string newString = curString.substr(0, curString.length() - 1);
				term->_input = newString;
				term->_redrawLineAt(term->_getLineCount() - 1);
			}
			break;
		}
	}
}

void Terminal::onTextInput(std::string text)
{
	if (doTakeInput())
	{
		Terminal* term = getTerminalInUse();
		Interpreter* interpreter = getInterpreterInUse();
		if (interpreter->getIsProgramRunning() && term->_isReading || interpreter->getIsProgramRunning() == false)
		{
			term->_input.append(text);
			term->_redrawLineAt(term->_getLineCount() - 1);
		}
	}
}

void Terminal::onWindowResized(int w, int h)
{
	static_lineFitCount = (int)std::floor((double)(h - static_padding * 2) / static_fontHeight);
	static_windowRect->w = w - static_padding * 2;
	static_windowRect->h = static_lineFitCount * static_fontHeight;
}
