#pragma once

#include <SDL2/SDL.h>

class Graphics
{
public:
	static SDL_Renderer* getRenderer();

	static void init(SDL_Renderer* _renderer);

	static int setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);
	static int setColor(SDL_Color color);
	static void getColor(Uint8* r, Uint8* g, Uint8* b, Uint8* a);
	static SDL_Color getColor();

	static int setClipRect(SDL_Rect* rect);

	static int clear();
	static void present();

	static int drawTexture(SDL_Texture* texture, SDL_Rect* srcrect, SDL_Rect* dstrect);

	static int drawPoint(int x, int y);
	static int drawPoints(const SDL_Point* points, int count);

	static int drawLine(int x1, int y1, int x2, int y2);
	static int drawLines(const SDL_Point* points, int count);

	static int drawRect(int x, int y, int w, int h);
	static int drawRect(SDL_Rect* rect);
	static int drawRects(const SDL_Rect* rects, int count);
	static int fillRect(int x, int y, int w, int h);
	static int fillRect(SDL_Rect* rect);
	static int fillRects(const SDL_Rect* rects, int count);
private:
	static SDL_Renderer* renderer;
};

