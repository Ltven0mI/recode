#include "Action.h"

Action::Action(int actionType)
{
	_actionType = actionType;
	isCompleted = false;
	result_bool = false;
	result_string = "";
	arg_string = "";
}

Action::~Action()
{
}


int Action::getActionType()
{
	return _actionType;
}