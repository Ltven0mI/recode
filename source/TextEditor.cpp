#include "TextEditor.h"
#include "Terminal.h"
#include "Graphics.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <cmath>


std::string TextEditor::static_basePath;
std::string TextEditor::static_openFilePath;
SDL_Rect*   TextEditor::static_windowRect;

int TextEditor::static_row;
int TextEditor::static_col;
int TextEditor::static_realCol;

int TextEditor::static_scrollX;
int TextEditor::static_scrollY;
int TextEditor::static_padding;

bool TextEditor::static_isActive;

int         TextEditor::static_fontSize;
TTF_Font*   TextEditor::static_font;
SDL_Color   TextEditor::static_fontColour;

int TextEditor::static_fontHeight;
int TextEditor::static_lineFitCount;
int TextEditor::static_startVisibleRow;
int TextEditor::static_endVisibleRow;

std::vector<std::string>    TextEditor::static_lines;
std::vector<SDL_Surface*>   TextEditor::static_lineSurfaces;
std::vector<SDL_Texture*>   TextEditor::static_lineTextures;


void TextEditor::init(int w, int h, int padding, int fontSize)
{
	static_basePath = SDL_GetBasePath();
	static_isActive = false;

	static_row = 0;
	static_col = 0;
	static_realCol = 0;

	static_scrollX = 0;
	static_scrollY = 0;

	static_padding = padding;
	static_fontSize = fontSize;

	static_fontHeight = 0;
	static_fontColour = SDL_Color { 0x00, 0x8f, 0xf0, 0x00 };
	static_font = openFont(DIR_DATA + "fonts/SourceCodePro-Regular.ttf");
	if (static_font == NULL)
	{
		std::cout << "[TextEditor] Failed to open font: " << static_basePath + DIR_DATA + "fonts/SourceCodePro-Regular.ttf" << std::endl;
		exit(1);
	}
	TTF_SizeText(static_font, " ", NULL, &static_fontHeight);

	static_lineFitCount = (int)std::floor((double)(h - static_padding * 2) / static_fontHeight);
	updateVisibleRows();

	static_windowRect = new SDL_Rect { padding, padding, w - padding * 2, static_lineFitCount * static_fontHeight };

    static_lines = std::vector<std::string>();
	static_lineSurfaces = std::vector<SDL_Surface*>();
	static_lineTextures = std::vector<SDL_Texture*>();
}


// Drawing //
void TextEditor::draw()
{
	if (static_isActive)
	{
		Graphics::setColor(10, 10, 10, 255);
		Graphics::fillRect(static_windowRect);
		Graphics::setClipRect(static_windowRect);

		SDL_Rect lineRect { 0, 0, 0, 0 };
		int lineWidth;
		for (int i = static_startVisibleRow; i < static_endVisibleRow; i++)
		{
			int loopIndex = i - static_startVisibleRow;

			std::string line = static_lines[i];
			TTF_SizeText(static_font, line.c_str(), &lineWidth, NULL);

			lineRect.x = static_windowRect->x;
			lineRect.y = static_windowRect->y + static_fontHeight * loopIndex;
			lineRect.w = lineWidth;
			lineRect.h = static_fontHeight;

			if (static_row == i)
			{
				Graphics::setColor(20, 20, 22, 255);
				SDL_Rect currentLineRect { static_windowRect->x, lineRect.y, static_windowRect->w, lineRect.h };
				Graphics::fillRect(&currentLineRect);

				int lineSubW;
				std::string lineSub = line.substr(0, static_col);
				TTF_SizeText(static_font, lineSub.c_str(), &lineSubW, NULL);

				Graphics::setColor(60, 60, 67, 255);
				SDL_Rect cursorRect { lineRect.x + lineSubW, lineRect.y, 2, lineRect.h };
				Graphics::fillRect(&cursorRect);
			}

            if (static_lineSurfaces[i] != NULL)
            {
                if (static_lineTextures[i] == NULL)
                    static_lineTextures[i] = SDL_CreateTextureFromSurface(Graphics::getRenderer(), static_lineSurfaces[i]);
				Graphics::drawTexture(static_lineTextures[i], NULL, &lineRect);
            }
		}

		Graphics::setClipRect(NULL);
	}
}


// Getters and Setters //
bool TextEditor::getIsActive()
{
	return static_isActive;
}

void TextEditor::setActive(bool active)
{
	static_isActive = active;
}

std::string TextEditor::getText()
{
	std::string result = "";
	for (int i = 0; i < getLineCount(); i++)
	{
		result.append(static_lines[i]);
		if (i < getLineCount())
			result.append("\n");
	}
	return result;
}

void TextEditor::setText(std::string text)
{
	clearText();
	std::istringstream stream(text);
	std::string line;
	while (std::getline(stream, line))
		addLine(line);
	if (getLineCount() == 0)
		addLine("");
}

int TextEditor::getLineCount()
{
	return (int)static_lines.size();
}


// Loaders //
TTF_Font* TextEditor::openFont(std::string path)
{
	std::string fullPath = static_basePath + path;
	return TTF_OpenFont(fullPath.c_str(), static_fontSize);
}

bool TextEditor::openFile(std::string path)
{
	clearText();

	std::string fullpath = static_basePath + path;
	std::fstream file;
	file.open(fullpath);
	if (file.is_open() == false)
	{
		addLine("");
		std::cout << "[TextEditor] Creating new file: " << fullpath << std::endl;
		file.close();
		static_openFilePath = path;
		return true;
	}

	std::string line;
	while (std::getline(file, line))
		addLine(line);

	file.close();

	static_openFilePath = path;
	return true;
}

bool TextEditor::saveFile()
{
    std::string fullpath = static_basePath + static_openFilePath;
	std::ofstream file (fullpath);
	if (file.is_open() == false)
	{
		std::cout << "[TextEditor] Failed to save file to: " << fullpath << std::endl;
		return false;
	}
	std::string text = getText();
	file << text;
	file.close();
	return true;
}


// Line Manipulators //
void TextEditor::addLine(std::string line)
{
	static_lines.push_back(line);
	static_lineSurfaces.push_back(TTF_RenderText_Blended(static_font, line.c_str(), static_fontColour));
	static_lineTextures.push_back(NULL);

	updateVisibleRows();
}

void TextEditor::insertLine(int index, std::string line)
{
	static_lines.insert(static_lines.begin() + index, line);
	static_lineSurfaces.insert(static_lineSurfaces.begin() + index, TTF_RenderText_Blended(static_font, line.c_str(), static_fontColour));
	static_lineTextures.insert(static_lineTextures.begin() + index, NULL);

	updateVisibleRows();
}

void TextEditor::setLineAt(int index, std::string line)
{
	// Cleanup
	SDL_FreeSurface(static_lineSurfaces[index]);
	if (static_lineTextures[index] != NULL)
		SDL_DestroyTexture(static_lineTextures[index]);

	// Assignment
	static_lines[index] = line;
	static_lineSurfaces[index] = TTF_RenderText_Blended(static_font, line.c_str(), static_fontColour);
	static_lineTextures[index] = NULL;
}

void TextEditor::removeLineAt(int index)
{
	// Cleanup
	SDL_FreeSurface(static_lineSurfaces[index]);
	if (static_lineTextures[index] != NULL)
		SDL_DestroyTexture(static_lineTextures[index]);

	// Removing
	static_lines.erase(static_lines.begin() + index);
	static_lineSurfaces.erase(static_lineSurfaces.begin() + index);
	static_lineTextures.erase(static_lineTextures.begin() + index);

	updateVisibleRows();
}


// Cursor Movers //
bool TextEditor::moveCursorUp()
{
	if (static_row > 0)
	{
		static_row--;
		int lineLength = static_lines[static_row].length();
		if (lineLength < static_realCol)
			static_col = lineLength;
		else
			static_col = static_realCol;
		onCursorMoved();
		return true;
	}
	return false;
}

bool TextEditor::moveCursorDown()
{
	if (static_row < getLineCount() - 1)
	{
		static_row++;
		int lineLength = static_lines[static_row].length();
		if (lineLength < static_realCol)
			static_col = lineLength;
		else
			static_col = static_realCol;
		onCursorMoved();
		return true;
	}
	return false;
}

bool TextEditor::moveCursorLeft()
{
	int lineLength = static_lines[static_row].length();
	if (static_col > 0)
	{
		if (static_realCol >= lineLength)
			static_realCol = lineLength - 1;
		else
			static_realCol--;
		static_col = static_realCol;
		return true;
	}
	else if (static_row > 0)
	{
		static_row--;
		lineLength = static_lines[static_row].length();
		static_realCol = lineLength;
		static_col = static_realCol;
		return true;
	}
	return false;
}

bool TextEditor::moveCursorRight()
{
	int lineLength = static_lines[static_row].length();
	if (static_col < lineLength)
	{
		static_col++;
		static_realCol = static_col;
		return true;
	}
	else if (static_row < getLineCount() - 1)
	{
		static_row++;
		static_realCol = 0;
		static_col = 0;
		return true;
	}
	return false;
}


// Scrollers //
bool TextEditor::scrollUp()
{
	if (static_scrollY > 0)
	{
		static_scrollY--;
		updateVisibleRows();
		return true;
	}
	return false;
}

bool TextEditor::scrollDown()
{
	if (static_scrollY < getLineCount() - 1)
	{
		static_scrollY++;
		updateVisibleRows();
		return true;
	}
	return false;
}

void TextEditor::scrollTo(int row)
{
	if (row < static_startVisibleRow)
		static_scrollY = row;
	else
		static_scrollY = row - static_lineFitCount + 1;
	updateVisibleRows();
}


// Utility Methods //
void TextEditor::clearText()
{
	static_row = 0;
	static_col = 0;
	static_realCol = 0;
	static_scrollX = 0;
	static_scrollY = 0;

	for (int i = 0; i < (int)static_lineSurfaces.size(); i++)
		SDL_FreeSurface(static_lineSurfaces[i]);
	for (int i = 0; i < (int)static_lineTextures.size(); i++)
		SDL_DestroyTexture(static_lineTextures[i]);

	static_lines.clear();
	static_lineSurfaces.clear();
	static_lineTextures.clear();
}

void TextEditor::updateVisibleRows()
{
	static_startVisibleRow = static_scrollY;
	static_endVisibleRow = (int)std::fmin(getLineCount(), static_scrollY + static_lineFitCount);
}

void TextEditor::onCursorMoved()
{
	if (static_row < static_startVisibleRow || static_row >= static_endVisibleRow)
		scrollTo(static_row);
}


// Callback Handlers //
void TextEditor::onKeyPressed(SDL_Keycode keycode, uint16_t mod)
{
	if (static_isActive == true)
	{
		int lineLength = static_lines[static_row].length();
		switch (keycode)
		{
		case SDLK_s:
            if (mod & KMOD_CTRL && static_openFilePath.empty() == false)
                saveFile();
            break;
        case SDLK_e:
			if (mod & KMOD_CTRL)
			{
				setActive(false);
				Terminal::setActive(true);
			}
            break;
		case SDLK_UP:
			moveCursorUp();
			break;
		case SDLK_DOWN:
			moveCursorDown();
			break;
		case SDLK_LEFT:
			moveCursorLeft();
			break;
		case SDLK_RIGHT:
			moveCursorRight();
			break;
		case SDLK_PAGEUP:
			scrollUp();
			break;
		case SDLK_PAGEDOWN:
			scrollDown();
			break;
		case SDLK_BACKSPACE:
			if (lineLength > 0 && static_col > 0)
			{
				std::string leftSub = static_lines[static_row].substr(0, static_col-1);
				std::string rightSub = static_lines[static_row].substr(static_col);
				setLineAt(static_row, leftSub + rightSub);
				static_col--;
				static_realCol = static_col;
			}
			else if (static_col == 0 && static_row > 0)
			{
				static_realCol = static_lines[static_row - 1].length();
				static_col = static_realCol;
				setLineAt(static_row - 1, static_lines[static_row - 1] + static_lines[static_row]);
				static_row--;
				removeLineAt(static_row + 1);
				onCursorMoved();
			}
			break;
		case SDLK_DELETE:
			if (static_col < lineLength)
			{
				std::string leftSub = static_lines[static_row].substr(0, static_col);
				std::string rightSub = static_lines[static_row].substr(static_col + 1);
				setLineAt(static_row, leftSub + rightSub);
			}
			else if (getLineCount() > static_row + 1)
			{
				setLineAt(static_row, static_lines[static_row] + static_lines[static_row + 1]);
				removeLineAt(static_row + 1);
				onCursorMoved();
			}
			break;
		case SDLK_RETURN:
			std::string leftSub = static_lines[static_row].substr(0, static_col);
			std::string rightSub = static_lines[static_row].substr(static_col);
			setLineAt(static_row, leftSub);
			insertLine(static_row + 1, rightSub);
			static_row++;
			static_col = 0;
			static_realCol = 0;
			onCursorMoved();
			break;
		}
	}
}

void TextEditor::onTextInput(std::string text)
{
	if (static_isActive == true)
	{
		std::string leftSub = static_lines[static_row].substr(0, static_col);
		std::string rightSub = static_lines[static_row].substr(static_col);
		setLineAt(static_row, leftSub + text + rightSub);
		static_col++;
		static_realCol = static_col;
	}
}

void TextEditor::onWindowResized(int w, int h)
{
	static_lineFitCount = (int)std::floor((double)(h - static_padding * 2) / static_fontHeight);
	static_windowRect->w = w - static_padding * 2;
	static_windowRect->h = static_lineFitCount * static_fontHeight;
	updateVisibleRows();
}
