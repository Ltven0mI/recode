#include "IdGenerator.h"

#include <iostream>
#include <cmath>

IdGenerator::IdGenerator(IdType idType)
{
	switch (idType)
	{
	case IdType::uint8:
		uint8Array = std::vector<uint8_t>((size_t)std::ceil((double)UINT8_MAX / 8), 0 );
		break;
	case IdType::uint16:
		uint8Array = std::vector<uint8_t>((size_t)std::ceil((double)UINT16_MAX / 8), 0);
		break;
	case IdType::uint32:
		uint8Array = std::vector<uint8_t>((size_t)std::ceil((double)UINT32_MAX / 8), 0);
		break;
	}
}

IdGenerator::~IdGenerator()
{
	uint8Array.clear();
}


bool IdGenerator::requestNewId(size_t* newId)
{
	
	size_t currentID = 0;
	while (currentID < uint8Array.size())
	{
		if (checkIdInUse(currentID) == false)
		{
			*newId = currentID;
			setIdAt(currentID, true);
			return true;
		}
		currentID++;
	}
	return false;
}

bool IdGenerator::requestSpecificId(size_t id)
{
	if (checkIdInUse(id) == false)
	{
		setIdAt(id, true);
		return true;
	}
	return false;
}

bool IdGenerator::checkIdInUse(size_t id)
{
	size_t byteIndex = (size_t)std::floor((double)id / 8);
	uint8_t bitIndex = (uint8_t)(id - byteIndex * 8);
	uint8_t byte = uint8Array[byteIndex];

	return (byte & (1 << bitIndex)) != 0;
}

void IdGenerator::freeId(size_t id)
{
	setIdAt(id, false);
}

void IdGenerator::setIdAt(size_t id, bool inuse)
{
	size_t byteIndex = (size_t)std::floor((double)id / 8);
	uint8_t bitIndex = (uint8_t)(id - byteIndex * 8);
	uint8_t byte = uint8Array[byteIndex];
	if (inuse == true)
	{
		uint8Array[byteIndex] |= (1 << bitIndex);
	}
	else
	{
		uint8Array[byteIndex] &= ~(1 << bitIndex);
	}
}
