#pragma once

#include <inttypes.h>
#include <vector>
#include <cstddef>

class IdGenerator
{
public:
	enum IdType { uint8, uint16, uint32 };

	IdGenerator(IdType idType);
	~IdGenerator();

	bool requestNewId(size_t* newId);
	bool requestSpecificId(size_t id);
	bool checkIdInUse(size_t id);
	void freeId(size_t id);
private:
	std::vector<uint8_t> uint8Array;
	void setIdAt(size_t id, bool inuse);
};
