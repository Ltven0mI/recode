#include "Graphics.h"

SDL_Renderer* Graphics::renderer;


SDL_Renderer* Graphics::getRenderer()
{
	return renderer;
}


void Graphics::init(SDL_Renderer* _renderer)
{
	renderer = _renderer;
}


int Graphics::setColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
	return SDL_SetRenderDrawColor(renderer, r, g, b, a);
}

int Graphics::setColor(SDL_Color color)
{
	return SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
}

void Graphics::getColor(Uint8* r, Uint8* g, Uint8* b, Uint8* a)
{
	SDL_GetRenderDrawColor(renderer, r, g, b, a);
}

SDL_Color Graphics::getColor()
{
	SDL_Color color { };
	SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &color.a);
	return color;
}


int Graphics::setClipRect(SDL_Rect* rect)
{
	return SDL_RenderSetClipRect(renderer, rect);
}


int Graphics::clear()
{
	return SDL_RenderClear(renderer);
}

void Graphics::present()
{
	SDL_RenderPresent(renderer);
}


int Graphics::drawTexture(SDL_Texture* texture, SDL_Rect* srcrect, SDL_Rect* dstrect)
{
	return SDL_RenderCopy(renderer, texture, srcrect, dstrect);
}


// Drawing Points //
int Graphics::drawPoint(int x, int y)
{
	return SDL_RenderDrawPoint(renderer, x, y);
}

int Graphics::drawPoints(const SDL_Point* points, int count)
{
	return SDL_RenderDrawPoints(renderer, points, count);
}


// Drawing Lines //
int Graphics::drawLine(int x1, int y1, int x2, int y2)
{
	return SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}

int Graphics::drawLines(const SDL_Point* points, int count)
{
	return SDL_RenderDrawLines(renderer, points, count);
}


// Drawing Rectangles //
int Graphics::drawRect(int x, int y, int w, int h)
{
	SDL_Rect rect { x, y, w, h };
	return SDL_RenderDrawRect(renderer, &rect);
}

int Graphics::drawRect(SDL_Rect* rect)
{
	return SDL_RenderDrawRect(renderer, rect);
}

int Graphics::drawRects(const SDL_Rect* rects, int count)
{
	return SDL_RenderDrawRects(renderer, rects, count);
}

// Filling Rectangles //
int Graphics::fillRect(int x, int y, int w, int h)
{
	SDL_Rect rect { x, y, w, h };
	return SDL_RenderFillRect(renderer, &rect);
}

int Graphics::fillRect(SDL_Rect* rect)
{
	return SDL_RenderFillRect(renderer, rect);
}

int Graphics::fillRects(const SDL_Rect* rects, int count)
{
	return SDL_RenderFillRects(renderer, rects, count);
}
