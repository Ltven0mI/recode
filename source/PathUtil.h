#pragma once

#include "StringUtil.h"

#include <string>
#include <vector>
#include <algorithm>

class PathUtil
{
public:
	static std::string validatePath(const std::string &path)
	{
		std::string pathInput = path;
		std::replace(pathInput.begin(), pathInput.end(), '\\', '/');
		pathInput.append("/");
		std::vector<std::string> pathSegments = StringUtil::split(pathInput, '/');

		for (size_t i = 0; i < pathSegments.size(); i++)
		{
			if (pathSegments[i] == "..")
			{
				if (i > 0)
					pathSegments[i - 1] = "";
				pathSegments[i] = "";
			}
		}

		std::string validatedPath = "";
		for (size_t i = 0; i < pathSegments.size(); i++)
		{
			if (pathSegments[i].empty() == false)
			{
				validatedPath.append(pathSegments[i]);
				if (i < pathSegments.size() - 1)
					validatedPath.append("/");
				else if (path.back() == '/')
					validatedPath.append("/");
			}
		}

		return validatedPath;
	}
};
