#pragma once
#include "Chunk.h"
#include "Tile.h"

#include <vector>
#include <string>
#include <SDL2/SDL.h>

class Tile;
class Chunk;
class Map
{
public:
	static void init(std::string saveName, int tileSize);
	static void init(std::string saveName, uint8_t width, uint8_t height, uint8_t chunkWidth, uint8_t chunkHeight, int tileSize);
	static void cleanup();
	
	static void update(double dt);
	static void draw();

	static int getWidth();
	static int getHeight();
	static int getChunkWidth();
	static int getChunkHeight();

	static Chunk* getChunk(int x, int y);

	static Tile* getActiveTile();
	static void setActiveTile(Tile* tile);

	static Tile* getTileAtWorldPosition(int worldX, int worldY);
	static bool addTileAtWorldPosition(int worldX, int worldY, Tile* tile);
	static void removeTileAtWorldPosition(int worldX, int worldY);

	static bool validateSaveName(std::string saveName);

	static std::string getSavePath();
	static std::string getFullSavePath();

	static bool saveMap(std::string saveName);

	static bool createTileSave(Tile* tile);

	static void worldToChunkSpace(int worldX, int worldY, int* chunkX, int* chunkY);
	static void worldToTileSpace(int worldX, int worldY, int* tileX, int* tileY);
private:
	static uint8_t _width;
	static uint8_t _height;
	static uint8_t _chunkWidth;
	static uint8_t _chunkHeight;
	static int _tileSize;

	static std::string _saveName;

	static Tile* _activeTile;

	static std::vector<std::vector<Chunk*> > chunkGrid;

	static void initChunks();

	static bool openSave(std::string saveName);
	static std::string serialize();
	static void deserialize(std::string serializedMap);
};

