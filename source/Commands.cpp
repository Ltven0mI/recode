#include "Commands.h"

#include "TextEditor.h"
#include "PathUtil.h"
#include "FileUtil.h"

#include <SDL2/SDL.h>
#include <stdexcept>

// edit Command //
std::vector<Argument> edit_args
{
    Argument ("filepath", "The file to edit")
};
static bool edit_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    if (args[0].front() == '/')
        relativePath = "";
    std::string validatedPath = PathUtil::validatePath(relativePath + args[0]);

    if (TextEditor::openFile(Map::getSavePath() + term->getGlobalPath() + validatedPath))
    {
        TextEditor::setActive(true);
        Terminal::setActive(false);
    }
    return true;
}
Command* edit_command = new Command("edit", edit_args, 1, &edit_run);

// ls Command //
std::vector<Argument> ls_args
{
    Argument ("path", "The directory to view")
};
static bool ls_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    std::string validatedPath = relativePath;

    if (args.size() > 0)
    {
        if (args[0].front() == '/')
            relativePath = "";
        validatedPath = PathUtil::validatePath(relativePath + args[0] + "/");
    }
    std::vector<std::string> programs = FileUtil::getDirectoryItems(SDL_GetBasePath() + Map::getSavePath() + term->getGlobalPath() + validatedPath);
    for (int i = 0; i < (int)programs.size(); i++)
    {
        term->addLine(programs[i]);
    }
    return true;
}
Command* ls_command = new Command("ls", ls_args, 0, &ls_run);

// cd Command //
std::vector<Argument> cd_args
{
    Argument ("path", "The directory to enter")
};
static bool cd_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    if (args[0].front() == '/')
        relativePath = "";
    std::string validatedPath = PathUtil::validatePath(relativePath + args[0] + "/");

    std::string fullpath = SDL_GetBasePath() + Map::getSavePath() + term->getGlobalPath() + validatedPath;
    if (FileUtil::isDirectory(fullpath) == true)
    {
        term->setRelativePath(validatedPath);
    }
    else
    {
        term->addLine("Failed to find the path specified.");
    }
    return true;
}
Command* cd_command = new Command("cd", cd_args, 1, &cd_run);

// rm Command //
std::vector<Argument> rm_args
{
    Argument ("filepath", "The file to remove")
};
static bool rm_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    if (args[0].front() == '/')
        relativePath = "";
    std::string validatedPath = PathUtil::validatePath(relativePath + args[0]);

    std::string fullPath = SDL_GetBasePath() + Map::getSavePath() + term->getGlobalPath() + validatedPath;
    if (FileUtil::deleteFile(fullPath) == false)
    {
        term->addLine("Could not find: '" + term->getRelativePath() + args[0] + "'");
    }
    return true;
}
Command* rm_command = new Command("rm", rm_args, 1, &rm_run);

// rmdir Command //
std::vector<Argument> rmdir_args
{
    Argument ("path", "The directory to remove")
};
static bool rmdir_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    if (args[0].front() == '/')
        relativePath = "";
    std::string validatedPath = PathUtil::validatePath(relativePath + args[0] + "/");

	if (term->getRelativePath() != validatedPath)
	{
		std::string fullPath = SDL_GetBasePath() + Map::getSavePath() + term->getGlobalPath() + validatedPath;
		if (FileUtil::deleteDirectory(fullPath) == false)
		{
			term->addLine("Failed to find the path specified.");
		}
	}
	else
	{
		term->addLine("Cannot remove current directory.");
	}

    return true;
}
Command* rmdir_command = new Command("rmdir", rmdir_args, 1, &rmdir_run);

// mkdir Command //
std::vector<Argument> mkdir_args
{
    Argument ("path", "The directory to create")
};
static bool mkdir_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    std::string relativePath = term->getRelativePath();
    if (args[0].front() == '/')
        relativePath = "";
    std::string validatedPath = PathUtil::validatePath(relativePath + args[0] + "/");

    std::string fullPath = SDL_GetBasePath() + Map::getSavePath() + term->getGlobalPath() + validatedPath;
    if (FileUtil::createDirectory(fullPath) == false)
    {
        term->addLine(FileUtil::getError());
    }
    return true;
}
Command* mkdir_command = new Command("mkdir", mkdir_args, 1, &mkdir_run);

// save Command //
std::vector<Argument> save_args
{
    Argument ("savename", "The name of the save")
};
static bool save_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    if (Map::validateSaveName(args[0]))
    {
        Map::saveMap(args[0]);
        return true;
    }
    term->addLine("err: savename contains invalid characters");
    return false;
}
Command* save_command = new Command("save", save_args, 1, &save_run);

// load Command //
std::vector<Argument> load_args
{
    Argument ("savename", "The name of the save")
};
static bool load_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    if (Map::validateSaveName(args[0]))
    {
        Map::init(args[0], 30);
        return true;
    }
    term->addLine("err: savename contains invalid characters");
    return false;
}
Command* load_command = new Command("load", load_args, 1, &load_run);

// help Command //
std::vector<Argument> help_args
{
    Argument ("command", "The command to get help for")
};
static bool help_run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
	/*
    help test
    [test] required (non-required)
    |- required : info
    |- non-required : data

    help
    [test] required (non-required)
    [rm] filepath
    [rmdir] path
	*/

    if (args.size() > 0)
    {
        Command* command = Commands::getCommand(args[0]);
        if (command != NULL)
        {
            term->addLine(command->getHelpString());
            for (int i = 0; i < (int)command->getArgs().size(); i++)
            {
                Argument arg = command->getArgs()[i];
                term->addLine("|- " + arg.name + " : " + arg.info);
            }
            return true;
        }
        term->addLine("Could not find help for '" + args[0] + "'");
        return false;
    }
    else
    {
        std::map<std::string, Command*> commandMap = Commands::getCommandMap();
        for (auto& entry : commandMap)
        {
			if (entry.second != NULL)
				term->addLine(entry.second->getHelpString());
        }
    }
    return true;
}
Command* help_command = new Command("help", help_args, 0, &help_run);


std::map<std::string, Command*> Commands::_commandMap;
bool Commands::_isInitialized = false;

void Commands::init()
{
	if (_isInitialized == false)
	{
		_isInitialized = true;
		std::cout << "Registering commands!" << std::endl;
		addCommand(edit_command);
		addCommand(ls_command);
		addCommand(cd_command);
		addCommand(rm_command);
		addCommand(rmdir_command);
		addCommand(mkdir_command);
		addCommand(save_command);
		addCommand(load_command);
		addCommand(help_command);
		std::cout << "Finished!" << std::endl;
	}
	else
	{
		throw std::logic_error("Attempted to initialize Commands, multiple times!");
	}
}


void Commands::addCommand(Command* command)
{
	if (_isInitialized == true)
		_commandMap[command->getTitle()] = command;
	else
		throw std::logic_error("Attempt to call 'addCommand' before initialization!");
}

Command* Commands::getCommand(std::string title)
{
	if (_isInitialized == true)
		return _commandMap[title];
	else
		throw std::logic_error("Attempt to call 'getCommand' before initialization!");
	return NULL;
}

std::map<std::string, Command*> Commands::getCommandMap()
{
    return _commandMap;
}
