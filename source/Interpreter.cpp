#include "Interpreter.h"
#include "Terminal.h"
#include "TextEditor.h"
#include "Keyboard.h"
#include "ltermlib.h"
#include "ltilelib.h"
#include "lshelllib.h"

#include <iostream>

Interpreter::Interpreter(Tile* tile)
{
	_tile = tile;
	_terminal = tile->getTerminal();

	_programCode = "";
	_isProgramRunning = false;
	_programContinue = false;
	_programShouldStart = false;

	_isThreadRunning = false;
	_threadContinue = false;

	_isDestroyed = false;

	_programMutex = SDL_CreateMutex();
	_threadMutex = SDL_CreateMutex();
	_thread = NULL;
}

Interpreter::Interpreter(Terminal* terminal)
{
	_tile = NULL;
	_terminal = terminal;

	_programCode = "";
	_isProgramRunning = false;
	_programContinue = false;
	_programShouldStart = false;

	_isThreadRunning = false;
	_threadContinue = false;

	_isDestroyed = false;

	_programMutex = SDL_CreateMutex();
	_threadMutex = SDL_CreateMutex();
	_thread = NULL;
}

Interpreter::~Interpreter()
{
	if (_isThreadRunning)
		stopThread();
	SDL_DestroyMutex(_programMutex);
	SDL_DestroyMutex(_threadMutex);
}


// Getters - Setters //
bool Interpreter::getIsThreadRunning()
{
	return _isThreadRunning;
}

bool Interpreter::getIsProgramRunning()
{
	return _isProgramRunning;
}

Tile* Interpreter::getTile()
{
	return _tile;
}

Terminal* Interpreter::getTerminal()
{
	return _terminal;
}


bool Interpreter::runProgram(std::string program, std::vector<std::string> args)
{
	if (_threadContinue == true)
	{
		if (_isProgramRunning == false && _programContinue == false && _programShouldStart == false && program.length() > 0)
		{
			SDL_LockMutex(_programMutex);	// Lock Program Mutex //
			_programCode = program;
			_programArgs = args;
			_programShouldStart = true;
			SDL_UnlockMutex(_programMutex);	// Unlock Program Mutex //
			return true;
		}
	}
	else
	{
		throw std::runtime_error("Tried to run program while thread isn't running!");
	}
	return false;
}

void Interpreter::terminateProgram()
{
	if (_programContinue == true)
	{
		SDL_LockMutex(_programMutex);	// Lock Program Mutex //
		_programContinue = false;
		SDL_UnlockMutex(_programMutex);	// Unlock Program Mutex //
	}
}


void Interpreter::startThread()
{
	if (_isThreadRunning == false && _threadContinue == false)
	{
		SDL_LockMutex(_threadMutex);	// Lock Thread Mutex //
		_threadContinue = true;
		SDL_UnlockMutex(_threadMutex);	// Unlock Thread Mutex //
		_thread = SDL_CreateThread(threadStart, "LuaInterpreterThread", (void*)this);
	}
	else
	{
		std::cout << "Failed to start Thread! Thread already running." << std::endl;
	}
}

void Interpreter::stopThread()
{
	if (_threadContinue == true)
	{
		SDL_LockMutex(_threadMutex);	// Lock Thread Mutex //
		_threadContinue = false;
		SDL_UnlockMutex(_threadMutex);	// Unlock Thread Mutex //
		terminateProgram();
		SDL_WaitThread(_thread, NULL);
	}
	else
	{
		std::cout << "Failed to stop Thread! Thread not running." << std::endl;;
	}
}


// Utility Methods //
Interpreter* Interpreter::getInterpreterFromLuaRegistry(lua_State* L)
{
	lua_pushstring(L, INTERPRETER_REGISTRYKEY);
	lua_gettable(L, LUA_REGISTRYINDEX);
	return (Interpreter*)lua_touserdata(L, -1);
}

void Interpreter::waitForAction(Action* action, int delayInterval)
{
	while (action->isCompleted == false && _threadContinue == true) SDL_Delay(delayInterval);
}


// Lua Methods //
int Interpreter::luaGetKeyDown(lua_State* L)
{
	if (lua_isstring(L, -1) == 1)
	{
		const char* key = lua_tostring(L, -1);
		lua_pushboolean(L, Keyboard::isDown(SDL_GetScancodeFromName(key)) && !Terminal::getIsActive() && !TextEditor::getIsActive());
		return 1;
	}
	std::string errorMsg = "Attempt to call 'getKeyDown(string)' with non string argument!";
	lua_pushlstring(L, errorMsg.c_str(), errorMsg.length());
	lua_error(L);
	return 0;
}

int Interpreter::luaRead(lua_State* L)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);
	Action* action = interpreter->_terminal->read();
	interpreter->waitForAction(action);
	std::string str = action->result_string;
	lua_pushlstring(L, str.c_str(), str.length());
	delete action;
	return 1;
}

int Interpreter::luaWrite(lua_State* L)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);
	int argCount = lua_gettop(L);

	for (int i = 1; i <= argCount; i++)
	{
		if (lua_isstring(L, i) == 1)
		{
			std::string text = lua_tostring(L, i);
			Action* writeAction;
			if (i < argCount - 1)
				writeAction = interpreter->_terminal->write(text + "    ");
			else
				writeAction = interpreter->_terminal->write(text);
			interpreter->waitForAction(writeAction);
			delete writeAction;
		}
	}
	return 0;
}

int Interpreter::luaPrint(lua_State* L)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);
	int argCount = lua_gettop(L);

	for (int i = 1; i <= argCount; i++)
	{
		if (lua_isstring(L, i) == 1)
		{
			std::string text = lua_tostring(L, i);
			Action* printAction;
			if (i < argCount - 1)
				printAction = interpreter->_terminal->write(text + "    ");
			else
				printAction = interpreter->_terminal->write(text);
			interpreter->waitForAction(printAction);
			delete printAction;
		}
	}
	Action* writeAction = interpreter->_terminal->write("\n");
	interpreter->waitForAction(writeAction);
	delete writeAction;
	return 0;
}

int Interpreter::luaExit(lua_State* L)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);
	interpreter->terminateProgram();
	return 0;
}


// Registers //
void Interpreter::registerLibs(lua_State* L)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);
	if (interpreter->_tile != NULL) luaopen_tile(L);
	luaopen_terminal(L);
	luaopen_shell(L);
}

void Interpreter::registerFunctions(lua_State* L)
{
	lua_register(L, "getKeyDown", luaGetKeyDown);
	lua_register(L, "read", luaRead);
	lua_register(L, "write", luaWrite);
	lua_register(L, "print", luaPrint);
	lua_register(L, "exit", luaExit);
}


// Lua Hook Handler //
void Interpreter::luaHookHandler(lua_State* L, lua_Debug* debug)
{
	Interpreter* interpreter = getInterpreterFromLuaRegistry(L);

	SDL_LockMutex(interpreter->_programMutex);	// Lock Program Mutex //
	bool shouldContinue = interpreter->_programContinue;
	SDL_UnlockMutex(interpreter->_programMutex);	// Unlock Program Mutex //

	if (shouldContinue == false)
		lua_error(L);
	SDL_Delay(5);
}

int Interpreter::threadStart(void* ptr)
{
	Interpreter* interpreter = (Interpreter*)ptr;

	SDL_LockMutex(interpreter->_threadMutex);	// Lock Thread Mutex //
	interpreter->_threadContinue = true;
	interpreter->_isThreadRunning = true;
	SDL_UnlockMutex(interpreter->_threadMutex);	// Unlock Thread Mutex //

	while (interpreter->_threadContinue)
	{
		if (interpreter->_programShouldStart == true)
		{
			SDL_LockMutex(interpreter->_programMutex);	// Lock Program Mutex //
			interpreter->_programShouldStart = false;
			interpreter->_isProgramRunning = true;
			interpreter->_programContinue = true;
			SDL_UnlockMutex(interpreter->_programMutex);	// Unlock Program Mutex //

			// Creating lua state - Handling failure
			lua_State* L = luaL_newstate();
			if (L == NULL)
				throw std::runtime_error("Failed to create new lua_State!");

			// Add Interpreter to Lua Registry
			lua_pushstring(L, INTERPRETER_REGISTRYKEY);
			lua_pushlightuserdata(L, interpreter);
			lua_settable(L, LUA_REGISTRYINDEX);


			// Opening default libs - Setting debug hook
			luaL_openlibs(L);
			lua_sethook(L, luaHookHandler, LUA_HOOKTAILCALL, 0);

			// Registering custom libs and functions
			registerLibs(L);
			registerFunctions(L);

			// Loading lua code -- Handle Errors
			int loadError = luaL_loadstring(L, interpreter->_programCode.c_str());
			if (loadError == LUA_ERRSYNTAX)
			{
				const char* error = lua_tostring(L, -1);
				interpreter->_terminal->write("[lua] ERROR:\n  " + std::string(error) + "\n");
			}
			else if (loadError == LUA_ERRMEM)
			{
				interpreter->_terminal->write("[lua] Failed to allocate memory for lua_State\n");
			}
			else if (loadError == 0)
			{
				if (interpreter->_programArgs.size() > 0)
				{
					for (int i = 0; i < (int)interpreter->_programArgs.size(); i++)
					{
						std::string arg = interpreter->_programArgs[i];
						lua_pushlstring(L, arg.c_str(), arg.size());
					}
				}

				int callingError = lua_pcall(L, interpreter->_programArgs.size(), LUA_MULTRET, NULL);
				if (interpreter->_programContinue && callingError != 0)
				{
					const char* error = lua_tostring(L, -1);
					interpreter->_terminal->write("[lua] ERROR:\n  " + std::string(error) + "\n");
				}
			}

			// Cleaning up
			lua_close(L);

			if (interpreter->_threadContinue)
			{
				Action* programEndAction = interpreter->_terminal->programEnded();
				interpreter->waitForAction(programEndAction);
				delete programEndAction;
			}

			SDL_LockMutex(interpreter->_programMutex);	// Lock Program Mutex //
			interpreter->_isProgramRunning = false;
			interpreter->_programContinue = false;
			SDL_UnlockMutex(interpreter->_programMutex);	// Unlock Program Mutex //
		}
		SDL_Delay(10);
	}

	SDL_LockMutex(interpreter->_threadMutex);	// Lock Thread Mutex //
	interpreter->_threadContinue = false;
	interpreter->_isThreadRunning = false;
	SDL_UnlockMutex(interpreter->_threadMutex);	// Unlock Thread Mutex //

	return 0;
}
