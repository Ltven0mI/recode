#include "Keyboard.h"


const Uint8* Keyboard::state;


void Keyboard::init()
{
	state = SDL_GetKeyboardState(NULL);
}

bool Keyboard::isDown(SDL_Scancode code)
{
	return state[code];
}
