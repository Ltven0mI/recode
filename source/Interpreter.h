#pragma once
#include "luainc.h"
#include "Tile.h"
#include "Terminal.h"
#include "Action.h"

#include <SDL2/SDL.h>
#include <vector>
#include <string>

#define INTERPRETER_REGISTRYKEY "recode-interpreter"

class Tile;
class Terminal;
class Interpreter
{
public:
	Interpreter(Tile* tile);
	Interpreter(Terminal* terminal);
	~Interpreter();

	bool getIsThreadRunning();
	bool getIsProgramRunning();

	Tile* getTile();
	Terminal* getTerminal();

	bool runProgram(std::string program, std::vector<std::string> args = { });
	void terminateProgram();

	void startThread();
	void stopThread();

	void waitForAction(Action* action, int delayInterval = 10);

	static Interpreter* getInterpreterFromLuaRegistry(lua_State* L);
private:
	Tile* _tile;
	Terminal* _terminal;

	std::string _programCode;
	std::vector<std::string> _programArgs;
	SDL_mutex* _programMutex;
	bool _isProgramRunning;
	bool _programContinue;
	bool _programShouldStart;

	SDL_Thread* _thread;
	SDL_mutex* _threadMutex;
	bool _isThreadRunning;
	bool _threadContinue;

	bool _isDestroyed;

	static int luaGetID(lua_State* L);
	static int luaGetKeyDown(lua_State* L);
	static int luaRead(lua_State* L);
	static int luaWrite(lua_State* L);
	static int luaPrint(lua_State* L);
	static int luaExit(lua_State* L);

	static void registerLibs(lua_State* L);
	static void registerFunctions(lua_State* L);

	static void luaHookHandler(lua_State* L, lua_Debug* debug);
	static int threadStart(void* ptr);
};

