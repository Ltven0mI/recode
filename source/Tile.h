#pragma once

#include "Chunk.h"
#include "Interpreter.h"
#include "Terminal.h"
#include "Action.h"
#include "IdGenerator.h"

#include <list>


#define TILE_DIR_UP 1
#define TILE_DIR_DOWN 2
#define TILE_DIR_LEFT 3
#define TILE_DIR_RIGHT 4


class Chunk;
class Interpreter;
class Terminal;
class Tile
{
public:
	uint8_t x;
	uint8_t y;

	uint8_t r;
	uint8_t g;
	uint8_t b;

	double drawOffsetX;
	double drawOffsetY;
	double moveSpeed;

	bool isActve;

	Chunk* chunk;

	Tile();
	Tile(uint16_t id);
	~Tile();

	void onAddedToMap();
	void onRemovedFromMap();

	void update(double dt);

	Action* moveUp();
	Action* moveDown();
	Action* moveLeft();
	Action* moveRight();

	Action* setColor(std::vector<int> colors);

	uint16_t getID();

	bool getIsMoving();
	int getMoveDirection();

	Interpreter* getInterpreter();
	Terminal* getTerminal();

	static IdGenerator idGenerator;
private:
	uint16_t _id;

	Interpreter* _interpreter;
	Terminal* _terminal;

	uint8_t _lockedX;
	uint8_t _lockedY;
	Chunk* _lockedChunk;

	std::list<Action*> _actions;
	Action* _currentAction;

	void addAction(Action* action);
	void doAction(Action* action);
	void onCompletedAction(bool result);

	bool _isMoving;
	double _movePercent;
	int _moveDirection;

	bool startMovingUp();
	bool startMovingDown();
	bool startMovingLeft();
	bool startMovingRight();
	void doSetColor();

	void onMoveUpFinished();
	void onMoveDownFinished();
	void onMoveLeftFinished();
	void onMoveRightFinished();
	void onMoveFinished();
};

