#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <stdio.h>


#define DIR_DATA (std::string)"data/"


#ifdef _WIN32
#include <Windows.h>

class FileUtil
{
public:
	static std::vector<std::string> getDirectoryItems(std::string path)
	{
		WIN32_FIND_DATA ffd;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		std::vector<std::string> folders;
		std::vector<std::string> files;

		hFind = FindFirstFile((path + "*").c_str(), &ffd);

		if (INVALID_HANDLE_VALUE == hFind)
		{
			return std::vector<std::string>{};
		}

		while (FindNextFile(hFind, &ffd) != 0)
		{
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
                std::string name = ffd.cFileName;
                size_t nameLen = name.length();
                if (nameLen != 2 || name[0] != '.' || name[1] != '.')
                {
                    folders.push_back(name);
                    folders.back().append("/");
                }
			}
			else
			{
				files.push_back(ffd.cFileName);
			}
		}
		FindClose(hFind);

		for (size_t i = 0; i < files.size(); i++)
		{
			folders.push_back(files[i]);
		}

		return folders;
	}

	static bool openFile(std::string path, std::string &contents)
	{
		std::ifstream stream(path, std::ios::in | std::ios::binary);
		if (stream.is_open() == true)
		{
			stream.seekg(0, std::ios::end);
			contents.resize((int)stream.tellg());
			stream.seekg(0, std::ios::beg);
			stream.read(&contents[0], contents.size());
			stream.close();
			return true;
		}
		return false;
	}

	static bool createDirectory(std::string directoryPath)
	{
		return CreateDirectory(directoryPath.c_str(), NULL) == 1;
	}

	static bool deleteFile(std::string path)
	{
		if (remove(path.c_str()) == 0)
			return true;
		return false;
	}

	static bool deleteDirectory(std::string path)
	{
		if (RemoveDirectory(path.c_str()) != 0)
			return true;
		return false;
	}

	static bool isDirectory(std::string path)
	{
		struct stat s;
		if (stat(path.c_str(), &s) == 0)
		{
			if (s.st_mode & S_IFDIR)
				return true;
			return false;
		}
		else
		{
			return false;
		}
	}

	static std::string getError()
	{
		switch (GetLastError())
		{
		case ERROR_ALREADY_EXISTS:
			return "The specified directory already exists.";
		case ERROR_PATH_NOT_FOUND:
			return "Failed to find the specified path.";
		default:
			std::ostringstream stream;
			stream << "Error: " << GetLastError();
			return stream.str();
		}
	}
};
#endif

#ifdef __unix__
#include <sys/stat.h>
#include <dirent.h>

class FileUtil
{
public:
	static std::vector<std::string> getDirectoryItems(std::string path)
	{
		std::vector<std::string> files;
		std::vector<std::string> folders;

        DIR* directory = opendir(path.c_str());
        if (directory != NULL)
        {
            dirent* entry = NULL;
            while ((entry = readdir(directory)) != NULL)
            {
                if (entry->d_type == DT_REG)
                {
                    files.push_back(entry->d_name);
                }
                else if (entry->d_type == DT_DIR)
                {
                    std::string name = entry->d_name;
                    size_t nameLen = name.length();
                    if (nameLen > 2 || (nameLen > 1 && name[0] != '.' && name[1] != '.')|| name[0] != '.')
                    {
                        folders.push_back(name);
                        folders.back().append("/");
                    }
                }
            }
            closedir(directory);

            for (size_t i=0; i < files.size(); i++)
            {
                folders.push_back(files[i]);
            }
            return folders;
        }
        else
        {
            return std::vector<std::string>{};
        }
	}

	static bool openFile(std::string path, std::string &contents)
	{
		std::ifstream stream(path, std::ios::in | std::ios::binary);
		if (stream.is_open() == true)
		{
			stream.seekg(0, std::ios::end);
			contents.resize((int)stream.tellg());
			stream.seekg(0, std::ios::beg);
			stream.read(&contents[0], contents.size());
			stream.close();
			return true;
		}
		return false;
	}

	static bool createDirectory(std::string directoryPath)
	{
		return mkdir(directoryPath.c_str(), 0700) == 0;
	}

	static bool deleteFile(std::string path)
	{
		if (remove(path.c_str()) == 0)
			return true;
		return false;
	}

	static bool deleteDirectory(std::string path)
	{
		if (remove(path.c_str()) == 0)
			return true;
		return false;
	}

	static bool isDirectory(std::string path)
	{
		struct stat s;
		if (stat(path.c_str(), &s) == 0)
		{
			if (s.st_mode & S_IFDIR)
				return true;
			return false;
		}
		else
		{
			return false;
		}
	}

	static std::string getError()
	{
        switch(errno)
        {
            case EEXIST:
                return "The specified directory already exists.";
            case ENOENT:
                return "Failed to find the specified path.";
            case ENOTDIR:
                return "Failed to find the specified path.";
            default:
                std::ostringstream stream;
                stream << "Error: " << errno;
                return stream.str();
        }
	}
};
#endif
