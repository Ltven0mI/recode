#pragma once

#include <SDL2/SDL.h>

class Camera
{
public:
	static void translate(int dx, int dy);
	static void setPosition(int x, int y);
	static void getPosition(int* x, int* y);

	static void screenToWorldSpace(int screenX, int screenY, int* worldX, int* worldY);
private:
	static int _x;
	static int _y;
};

