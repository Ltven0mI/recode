#pragma once

#include "Terminal.h"
#include "Command.h"

#include <map>

class Terminal;
class Command;
class Commands
{
public:
    static void init();

    static void addCommand(Command* command);
    static Command* getCommand(std::string name);
    static std::map<std::string, Command*> getCommandMap();
private:
    static std::map<std::string, Command*> _commandMap;
	static bool _isInitialized;
};
