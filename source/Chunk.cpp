#include "Chunk.h"
#include "Graphics.h"

#include <iostream>
#include <cmath>

Chunk::Chunk()
{
	
}

Chunk::Chunk(int x, int y, int width, int height)
{
	_x = x;
	_y = y;
	_width = width;
	_height = height;

	init();
}

Chunk::~Chunk()
{
}


void Chunk::init()
{
	tileList = std::list<Tile*>();
	tileRemoveList = std::list<Tile*>();
	tileGrid.resize(_width);
	for (int x = 0; x < _width; x++){
		tileGrid[x].resize(_height);
		for (int y = 0; y < _height; y++)
		{
			tileGrid[x][y] = NULL;
		}
	}
}


void Chunk::update(double dt)
{
	std::list<Tile*>::const_iterator iterator, end;
	for (iterator = tileList.begin(), end = tileList.end(); iterator != end; ++iterator)
	{
		Tile* tile = *iterator;
		tile->update(dt);
	}

	if (tileRemoveList.size() > 0)
	{
		for (iterator = tileRemoveList.begin(), end = tileRemoveList.end(); iterator != end; ++iterator)
		{
			Tile* tile = *iterator;
			tileList.remove(tile);
		}
		tileRemoveList.clear();
	}
}

void Chunk::draw(int x, int y, int tileSize)
{
	SDL_Rect chunkRect { x, y, _width * tileSize, _height * tileSize };
	Graphics::setColor(0, 143, 240, 255);
	Graphics::drawRect(&chunkRect);

	SDL_Rect tileRect { 0, 0, tileSize, tileSize };
	std::list<Tile*>::const_iterator iterator, end;
	for (iterator = tileList.begin(), end = tileList.end(); iterator != end; ++iterator)
	{
		Tile* tile = *iterator;
		tileRect.x = x + (int)std::floor((tile->x + tile->drawOffsetX) * tileSize);
		tileRect.y = y + (int)std::floor((tile->y + tile->drawOffsetY) * tileSize);
		Graphics::setColor(tile->r, tile->g, tile->b, 255);
		Graphics::fillRect(&tileRect);
		if (tile->isActve)
		{
			Graphics::setColor(255, 255, 255, 255);
			Graphics::drawRect(tileRect.x - 2, tileRect.y - 2, tileRect.w + 4, tileRect.h + 4);
		}
	}
}


bool Chunk::addTile(int x, int y, Tile* tile, bool addToMap)
{
	if (tileGrid[x][y] != NULL) return false;
	tile->x = x;
	tile->y = y;
	tile->chunk = this;
	tileGrid[x][y] = tile;
	tileList.push_back(tile);
	if (addToMap == true)
		tile->onAddedToMap();
	return true;
}

void Chunk::removeTileAt(int x, int y, bool removeFromMap)
{
	Tile* tile = tileGrid[x][y];
	if (tile != NULL)
	{
		tileRemoveList.push_back(tile);
		// These next things might need to me done after removing the tile from "tileList"
		tileGrid[x][y] = NULL;
		tile->chunk = NULL;
		if (removeFromMap == true)
			tile->onRemovedFromMap();
	}
}

void Chunk::removeTileAtImmediate(int x, int y, bool removeFromMap)
{
	Tile* tile = tileGrid[x][y];
	if (tile != NULL)
	{
		tileList.remove(tile);
		tileGrid[x][y] = NULL;
		tile->chunk = NULL;
		if (removeFromMap == true)
			tile->onRemovedFromMap();
	}
}


bool Chunk::lockArea(int x, int y, int w, int h, Tile* tile)
{
	for (int ix = x; ix < x + w; ix++)
	{
		for (int iy = y; iy < y + h; iy++)
		{
			Tile* curTile = tileGrid[ix][iy];
			if (curTile != NULL && curTile != tile)
			{
				return false;
			}
		}
	}

	for (int ix = x; ix < x + w; ix++)
	{
		for (int iy = y; iy < y + h; iy++)
		{
			if (tileGrid[ix][iy] != tile)
				tileGrid[ix][iy] = tile;
		}
	}

	return true;
}

bool Chunk::lockTile(int x, int y, Tile* tile)
{
	if (tileGrid[x][y] != NULL && tileGrid[x][y] != tile) return false;
	tileGrid[x][y] = tile;
	return true;
}

void Chunk::unlockTile(int x, int y)
{
	Tile* tile = tileGrid[x][y];
	if (tile != NULL)
		tileGrid[x][y] = NULL;
}


int Chunk::getX()
{
	return _x;
}

int Chunk::getY()
{
	return _y;
}

int Chunk::getWidth()
{
	return _width;
}

int Chunk::getHeight()
{
	return _height;
}


Tile* Chunk::getTile(int x, int y)
{
	return tileGrid[x][y];
}


Chunk* Chunk::getChunkUp()
{
	if (hasChunkUp() == false)
		return NULL;
	return Map::getChunk(_x, _y - 1);
}

Chunk* Chunk::getChunkDown()
{
	if (hasChunkDown() == false)
		return NULL;
	return Map::getChunk(_x, _y + 1);
}

Chunk* Chunk::getChunkLeft()
{
	if (hasChunkLeft() == false)
		return NULL;
	return Map::getChunk(_x - 1, _y);
}

Chunk* Chunk::getChunkRight()
{
	if (hasChunkRight() == false)
		return NULL;
	return Map::getChunk(_x + 1, _y);
}


bool Chunk::hasChunkUp()
{
	return (_y > 0);
}

bool Chunk::hasChunkDown()
{
	return (_y < Map::getHeight() - 1);
}

bool Chunk::hasChunkLeft()
{
	return (_x > 0);
}

bool Chunk::hasChunkRight()
{
	return (_x < Map::getWidth() - 1);
}

