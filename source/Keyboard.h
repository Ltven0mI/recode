#pragma once

#include <SDL2/SDL.h>


#define KEY_W SDL_SCANCODE_W
#define KEY_S SDL_SCANCODE_S
#define KEY_A SDL_SCANCODE_A
#define KEY_D SDL_SCANCODE_D
#define KEY_UP SDL_SCANCODE_UP
#define KEY_DOWN SDL_SCANCODE_DOWN
#define KEY_LEFT SDL_SCANCODE_LEFT
#define KEY_RIGHT SDL_SCANCODE_RIGHT


class Keyboard
{
public:
	static void init();

	static bool isDown(SDL_Scancode code);

private:
	static const Uint8* state;
};

