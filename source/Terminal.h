#pragma once
#include "Action.h"
#include "Tile.h"
#include "Interpreter.h"
#include "Commands.h"
#include "FileUtil.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <vector>
#include <list>

class Tile;
class Interpreter;
class Commands;
class Terminal
{
public:
	//Terminal(Tile* tile);
	Terminal(Tile* tile, std::string user, std::string globalPath, std::string homePath = "home/");
	Terminal(std::string user, std::string globalPath, std::string homePath = "home/");
	~Terminal();

	bool getIsRoot();
	std::string getUser();

	std::string getGlobalPath();
	std::string getHomePath();
	std::string getRelativePath();

	void setRelativePath(std::string relativePath);

	Action* read();
	Action* write(std::string text);
	Action* clear();

	Action* programEnded();

	void update(double dt);

	void addLine(std::string line);
	void startNewCommandLine();



	// STATIC //
	static void init(int w, int h, int maxLineCount = 50, int padding = 25, int fontSize = 18);
	static void cleanup();

	static void setTerminal(Terminal* terminal);

	static bool getIsActive();
	static void setActive(bool active);

	static Terminal* getRootTerminal();

	static void draw();

	static void onKeyPressed(SDL_Keycode keycode, uint16_t mod);
	static void onTextInput(std::string text);
	static void onWindowResized(int w, int h);
private:
	bool _isActiveTerminal;
	bool _isReading;
	bool _isRoot;
	Tile* _tile;


	std::string _globalPath;
	std::string _homePath;
	std::string _relativePath;

	std::string _user;


	std::vector<std::string>	_lines;
	std::vector<SDL_Surface*>	_lineSurfaces;
	std::vector<SDL_Texture*>	_lineTextures;
	std::string	_input;

	SDL_mutex* _actionMutex;
	std::list<Action*> _actions;
	Action* _currentAction;

	int	_scroll;

	void _addMultiline(std::string text);
	void _appendToLine(int index, std::string text);
	void _removeLastLine();
	int _getLineCount();

	void _addAction(Action* action);
	void _doAction(Action* action);
	void _onCompletedAction(std::string result);
	void _clearActions();

	bool _startReading();
	void _onReadingFinished();
	void _doWrite();
	void _doClear();
	void _doProgramEnded();

	void _imediateClear();

	bool _scrollUp();
	bool _scrollDown();
	void _scrollTo(int scroll);

	void _redrawLineAt(int index);
	void _redrawLineRange(int start, int count);

	// STATIC //
	static std::string	static_basePath;
	static bool			static_isActive;

	static int			static_lineFitCount;
	static int			static_maxLineCount;

	static int			static_padding;
	static int			static_fontSize;
	static int			static_fontHeight;
	static Terminal*	static_activeTerminal;
	static SDL_Color	static_fontColour;
	static TTF_Font*	static_font;
	static SDL_Rect*	static_windowRect;

	static Terminal*	static_rootTerminal;
	static Interpreter*	static_rootInterpreter;

	static bool			doTakeInput();
	static Terminal*	getTerminalInUse();
	static Interpreter*	getInterpreterInUse();

	static TTF_Font*	openFont(std::string path);
	static bool			openFile(std::string path, std::string &contents);

	static bool			parseCommand(std::string command, Terminal* term, Interpreter* interpreter);
};
