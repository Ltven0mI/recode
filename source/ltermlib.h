#pragma once

#include "Interpreter.h"
#include "Tile.h"
#include "Action.h"
#include "luainc.h"


int term_clear(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	Action* clearAction = interpreter->getTerminal()->clear();
	interpreter->waitForAction(clearAction);
	delete clearAction;
	return 0;
}

int term_getUser(lua_State* L)
{
    Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
    std::string user = interpreter->getTerminal()->getUser();
    lua_pushlstring(L, user.c_str(), user.length());
	return 1;
}


static const luaL_Reg termlib[] = {
	{ "clear", term_clear },
	{ "getUser", term_getUser },
	{ NULL, NULL }
};


LUAMOD_API int luaopen_terminal(lua_State *L)
{
	luaL_newlib(L, termlib);
	lua_setglobal(L, "term");
	return 1;
}
