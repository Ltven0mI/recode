#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

#include "Graphics.h"
#include "Keyboard.h"
#include "Map.h"
#include "Camera.h"
#include "TextEditor.h"

#include "FileUtil.h"

#include <iostream>
#include <cmath>


const int WIDTH = 800;
const int HEIGHT = 450;
const int FPS = 60;
const char* TITLE = "Recode\\__  v0.10a";

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

int main(int argc, char* argv[])
{
	// Initializing SDL //
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s", SDL_GetError());
		return 3;
	}
	SDL_StartTextInput();

	// Initializing TTF //
	if (TTF_Init() < 0)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize TTF: %s", TTF_GetError());
		return 3;
	}

	// Initializing IMG //
	int flags = IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF;
	int initted = IMG_Init(flags);
	if (initted&flags != flags)
	{
		printf("IMG_Init: Failed to init required jpg, png and tif support!\n");
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "IMG_Init: %s\n", IMG_GetError());
		return 3;
	}


	// Creating Window //
	window = SDL_CreateWindow(
        TITLE,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_RESIZABLE
    );
    if (window == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create window: %s", SDL_GetError());
        return 3;
    }

	// Loading Window Icon //
	std::string iconPath = SDL_GetBasePath() + DIR_DATA + "graphics/icon.png";
	SDL_Surface* windowIcon;
	windowIcon = IMG_Load(iconPath.c_str());
	if (windowIcon == NULL)
	{
		std::cout << "Failed to load window icon:" << std::endl;
		printf("| - %s\n", IMG_GetError());
	}
	else
	{
		SDL_SetWindowIcon(window, windowIcon);
        SDL_FreeSurface(windowIcon);
	}

	// Creating Renderer //
	renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not create renderer: %s", SDL_GetError());
        return 3;
    }


	// Initializing Recode //
	Graphics::init(renderer);
	Keyboard::init();
	TextEditor::init(WIDTH, HEIGHT);
	Terminal::init(WIDTH, HEIGHT);

	Map::init("test", 30);

	Tile* selectedTile = NULL;

	// Main Loop //
	SDL_Event event;
	Uint32 starting_tick = SDL_GetTicks();
	Uint32 last_tick = starting_tick;
	bool isRunning(true);
	while (isRunning)
	{
		starting_tick = SDL_GetTicks();
		while(SDL_PollEvent(&event)){
			switch (event.type)
			{
			case SDL_WINDOWEVENT:
				if (event.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					TextEditor::onWindowResized(event.window.data1, event.window.data2);
					Terminal::onWindowResized(event.window.data1, event.window.data2);
				}
				break;
			case SDL_KEYDOWN:
				TextEditor::onKeyPressed(event.key.keysym.sym, event.key.keysym.mod);
				Terminal::onKeyPressed(event.key.keysym.sym, event.key.keysym.mod);
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					//if (textEditor.getIsActive() == true)
						//textEditor.setActive(false);
					if (Terminal::getIsActive() == true)
						Terminal::setActive(false);
				}
				else if (event.key.keysym.sym == SDLK_TAB)
				{
					if (Terminal::getIsActive() == false && TextEditor::getIsActive() == false)
					{
						Terminal::setActive(true);
					}
				}
				break;
			case SDL_TEXTINPUT:
				TextEditor::onTextInput(event.text.text);
				Terminal::onTextInput(event.text.text);
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (TextEditor::getIsActive() == false && Terminal::getIsActive() == false)
				{
					int worldX, worldY;
					Camera::screenToWorldSpace(event.button.x, event.button.y, &worldX, &worldY);
					if (event.button.button == SDL_BUTTON_LEFT)
					{
						Tile* clickedTile = Map::getTileAtWorldPosition(worldX, worldY);
						if (clickedTile == NULL)
						{
							Tile* tileToAdd = new Tile();
							if (Map::addTileAtWorldPosition(worldX, worldY, tileToAdd) == false)
							{
								delete tileToAdd;
							}
						}
						else
						{
							delete clickedTile;
						}
					}
					else if (event.button.button == SDL_BUTTON_RIGHT)
					{
						selectedTile = Map::getTileAtWorldPosition(worldX, worldY);
						if (selectedTile != NULL)
						{
							//Terminal::setTerminal(selectedTile->_terminal);
							//Terminal::setActive(true);
							Map::setActiveTile(selectedTile);
						}
						else
						{
							Map::setActiveTile(NULL);
						}
					}
					else if (event.button.button == SDL_BUTTON_MIDDLE)
					{
						Map::saveMap("test");
					}
				}
				break;
			case SDL_QUIT:
				isRunning = false;
				break;
			}
		}

		// Input //
		if (TextEditor::getIsActive() == false && Terminal::getIsActive() == false)
		{
			if (Keyboard::isDown(KEY_UP))
			{
				Camera::translate(0, -10);
			}
			if (Keyboard::isDown(KEY_DOWN))
			{
				Camera::translate(0, 10);
			}

			if (Keyboard::isDown(KEY_LEFT))
			{
				Camera::translate(-10, 0);
			}
			if (Keyboard::isDown(KEY_RIGHT))
			{
				Camera::translate(10, 0);
			}
		}

		// Update //
		double delta = (starting_tick - last_tick) / 1000.0;
		Map::update(delta);
		Terminal::getRootTerminal()->update(delta);

		// Drawing //
		Graphics::setColor(0, 0, 0, 255);
		Graphics::clear();

		Map::draw();
		TextEditor::draw();
		Terminal::draw();

		Graphics::present();


		last_tick = starting_tick;

		// FPS Limit //
		if ((1000 / FPS) > SDL_GetTicks() - starting_tick)
		{
			SDL_Delay(1000 / FPS - (SDL_GetTicks() - starting_tick));
		}
	}

	// Cleaning Up //
	Terminal::cleanup();
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_StopTextInput();
	IMG_Quit();
	TTF_Quit();
	SDL_Quit();

	return 0;
}
