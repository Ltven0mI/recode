#pragma once

#include "Tile.h"
#include "Map.h"

#include <SDL2/SDL.h>
#include <list>
#include <vector>

class Tile;
class Map;
class Chunk
{
public:
	std::list<Tile*> tileList;
	std::list<Tile*> tileRemoveList;
	std::vector<std::vector<Tile*> > tileGrid;

	Chunk();
	Chunk(int x, int y, int width, int height);
	~Chunk();


	void update(double dt);
	void draw(int x, int y, int tileSize);


	bool addTile(int x, int y, Tile* tile, bool addToMap = true);
	void removeTileAt(int x, int y, bool removeFromMap = true);

	void removeTileAtImmediate(int x, int y, bool removeFromMap = true);


	bool lockArea(int x, int y, int w, int h, Tile* tile);
	bool lockTile(int x, int y, Tile* tile);
	void unlockTile(int x, int y);


	int getX();
	int getY();
	int getWidth();
	int getHeight();


	Tile* getTile(int x, int y);


	Chunk* getChunkUp();
	Chunk* getChunkDown();
	Chunk* getChunkLeft();
	Chunk* getChunkRight();

	bool hasChunkUp();
	bool hasChunkDown();
	bool hasChunkLeft();
	bool hasChunkRight();

private:
	int _x;
	int _y;
	int _width;
	int _height;

	void init();
};
