#include "Camera.h"

#include <iostream>


int Camera::_x;
int Camera::_y;


void Camera::translate(int dx, int dy)
{
	_x += dx;
	_y += dy;
}

void Camera::setPosition(int x, int y)
{
	_x = x;
	_y = y;
}

void Camera::getPosition(int* x, int* y)
{
	*x = _x;
	*y = _y;
}

void Camera::screenToWorldSpace(int screenX, int screenY, int* worldX, int* worldY)
{
	*worldX = screenX + _x;
	*worldY = screenY + _y;
}
