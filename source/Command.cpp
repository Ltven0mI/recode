#include "Command.h"


Command::Command(std::string title, std::vector<Argument> args, uint16_t requiredArgCount, bool (*function)(std::vector<std::string>, Terminal*, Interpreter*))
{
    _title = title;
    _args = args;
    _requiredArgCount = requiredArgCount;
    _function = function;

    _helpString = "[" + title + "] ";
    for (int i = 0; i < _args.size(); i++)
    {
        if (i >= _requiredArgCount)
            _helpString.append("(");
        _helpString.append(_args[i].name);
        if (i >= _requiredArgCount)
            _helpString.append(")");
        if (i < _args.size() - 1)
            _helpString.append(" ");
    }
}

Command::~Command()
{
}


int Command::run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter)
{
    if (args.size() >= _requiredArgCount)
    {
        if (_function(args, term, interpreter))
            return 0;
        return ERROR_COMMAND_FAILED;
    }
    return ERROR_COMMAND_MISSINGARGS;
}


std::string Command::getTitle()
{
    return _title;
}

std::vector<Argument> Command::getArgs()
{
    return _args;
}

uint16_t Command::getRequiredArgCount()
{
    return _requiredArgCount;
}

std::string Command::getHelpString()
{
    return _helpString;
}
