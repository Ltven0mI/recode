#pragma once

#include "Interpreter.h"
#include "Tile.h"
#include "Action.h"
#include "luainc.h"


int tile_getID(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	lua_pushinteger(L, interpreter->getTile()->getID());
	return 1;
}

int tile_moveup(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	Action* action = interpreter->getTile()->moveUp();
	interpreter->waitForAction(action);
	lua_pushboolean(L, action->result_bool);
	delete action;
	return 1;
}


int tile_movedown(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	Action* action = interpreter->getTile()->moveDown();
	interpreter->waitForAction(action);
	lua_pushboolean(L, action->result_bool);
	delete action;
	return 1;
}

int tile_moveleft(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	Action* action = interpreter->getTile()->moveLeft();
	interpreter->waitForAction(action);
	lua_pushboolean(L, action->result_bool);
	delete action;
	return 1;
}

int tile_moveright(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	Action* action = interpreter->getTile()->moveRight();
	interpreter->waitForAction(action);
	lua_pushboolean(L, action->result_bool);
	delete action;
	return 1;
}


int tile_setcolor(lua_State* L)
{
	Interpreter* interpreter = Interpreter::getInterpreterFromLuaRegistry(L);
	int argCount = lua_gettop(L);

	if (argCount > 0)
	{
		std::vector<int> colors;
		for (int i = 1; i < std::fmin(argCount, 3); i++)
		{
			if (lua_isinteger(L, i) == 1)
			{
				colors.push_back(lua_tointeger(L, i));
			}
			else if (lua_isnil(L, i) == 1)
			{
				colors.push_back(-1);
			}
			else
			{
				std::string errorMsg = "Attempt to call 'tile.setcolor(r, g, b)' with non number arguments!";
				lua_pushlstring(L, errorMsg.c_str(), errorMsg.length());
				lua_error(L);
				return 0;
			}
		}

		Action* action = interpreter->getTile()->setColor(colors);
		interpreter->waitForAction(action);
		delete action;
		return 1;
	}

	std::string errorMsg = "Attempt to call 'tile.setcolor(r, g, b)' with invalid arguments!";
	lua_pushlstring(L, errorMsg.c_str(), errorMsg.length());
	lua_error(L);
	return 0;
}


static const luaL_Reg tilelib[] = {
	{ "getID", tile_getID },
	{ "moveUp", tile_moveup },
	{ "moveDown", tile_movedown },
	{ "moveLeft", tile_moveleft },
	{ "moveRight", tile_moveright },
	{ "setcolor", tile_setcolor },
	{ NULL, NULL }
};


LUAMOD_API int luaopen_tile(lua_State *L)
{
	luaL_newlib(L, tilelib);
	lua_setglobal(L, "tile");
	return 1;
}
