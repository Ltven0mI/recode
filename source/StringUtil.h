#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <iostream>

class StringUtil
{
public:
	static void split(const std::string &string, char delim, std::vector<std::string> &elements)
	{
		std::stringstream ss;
		ss.str(string);
		std::string element;
		while (std::getline(ss, element, delim))
		{
			if (element.empty() == false) // Not Empty String
				elements.push_back(element);
		}
	}


	static std::vector<std::string> split(const std::string &string, char delim)
	{
		std::vector<std::string> elements;
		split(string, delim, elements);
		return elements;
	}
};
