#pragma once

#include <string>
#include <vector>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

class TextEditor
{
public:
	static void init(int w, int h, int padding = 25, int fontSize = 18);

	static void draw();

	static bool getIsActive();
	static void setActive(bool active);

	static std::string getText();
	static void setText(std::string text);

	static int getLineCount();

	static TTF_Font* openFont(std::string path);
	static bool openFile(std::string path);
	static bool saveFile();

	static void addLine(std::string line);
	static void insertLine(int index, std::string line);
	static void setLineAt(int index, std::string line);
	static void removeLineAt(int index);

	static bool moveCursorUp();
	static bool moveCursorDown();
	static bool moveCursorLeft();
	static bool moveCursorRight();

	static bool scrollUp();
	static bool scrollDown();
	static void scrollTo(int row);

	static void onKeyPressed(SDL_Keycode keycode, uint16_t mod);
	static void onTextInput(std::string text);
	static void onWindowResized(int w, int h);

private:
	static std::string static_basePath;
	static std::string static_openFilePath;

	static SDL_Rect* static_windowRect;
	static int static_padding;

	static int static_row;
	static int static_col;
	static int static_realCol;

	static int static_scrollX;
	static int static_scrollY;

	static bool static_isActive;

	static int static_fontSize;
	static TTF_Font* static_font;
	static SDL_Color static_fontColour;

	static int static_fontHeight;
	static int static_lineFitCount;
	static int static_startVisibleRow;
	static int static_endVisibleRow;

	static std::vector<std::string> static_lines;
	static std::vector<SDL_Surface*> static_lineSurfaces;
	static std::vector<SDL_Texture*> static_lineTextures;

	static void clearText();
	static void updateVisibleRows();
	static void onCursorMoved();

};

