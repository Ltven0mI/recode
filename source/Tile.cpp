#include "Tile.h"

#include <iostream>

IdGenerator Tile::idGenerator { IdGenerator::IdType::uint16 };

Tile::Tile()
{
	x = 0;
	y = 0;

	r = 0;
	g = 143;
	b = 240;

	_lockedX = 0;
	_lockedY = 0;
	_lockedChunk = NULL;

	_id;
	if (idGenerator.requestNewId((size_t*)&_id) == false)
	{
		std::cout << "Failed to request new id for tile! No id avaliable out of: " << UINT16_MAX << " ids." << std::endl;
		exit(1);
	}

	drawOffsetX = 0;
	drawOffsetY = 0;
	moveSpeed = 10.0;

	isActve = false;

	chunk = NULL;

	_terminal = new Terminal(this, std::to_string(_id), "tiles/" + std::to_string(_id) + "/");
	_interpreter = new Interpreter(this);
	//_interpreter->startThread();

	_actions = std::list<Action*>();
	_currentAction = NULL;

	_isMoving = false;
	_movePercent = 0;
	_moveDirection = 0;
}

Tile::Tile(uint16_t id)
{
	x = 0;
	y = 0;

	r = 0;
	g = 143;
	b = 240;

	_lockedX = 0;
	_lockedY = 0;
	_lockedChunk = NULL;

	_id = id;
	if (idGenerator.requestSpecificId(id) == false)
	{
		std::cout << "Failed to request id: " << id << " for tile! Not avaliable." << std::endl;
		exit(1);
	}

	drawOffsetX = 0;
	drawOffsetY = 0;
	moveSpeed = 10.0;

	isActve = false;

	chunk = NULL;

	_terminal = new Terminal(this, std::to_string(_id), "tiles/" + std::to_string(_id) + "/");
	_interpreter = new Interpreter(this);
	//_interpreter->startThread();

	_actions = std::list<Action*>();
	_currentAction = NULL;

	_isMoving = false;
	_movePercent = 0;
	_moveDirection = 0;
}

Tile::~Tile()
{
	if (isActve)
		Map::setActiveTile(NULL);

	delete _interpreter;
	delete _terminal;

	if (_lockedChunk != NULL)
		_lockedChunk->unlockTile(_lockedX, _lockedY);
	if (chunk != NULL)
		chunk->removeTileAtImmediate(x, y);

	_actions.clear();

	_isMoving = false;
	_movePercent = 0;
	_moveDirection = 0;

	idGenerator.freeId(_id);
}


void Tile::onAddedToMap()
{
	if (_interpreter->getIsThreadRunning() == false)
		_interpreter->startThread();
}

void Tile::onRemovedFromMap()
{
	if (_interpreter->getIsThreadRunning() == true)
		_interpreter->stopThread();
}


void Tile::update(double dt)
{
	_terminal->update(dt);

	if (_actions.size() > 0 && _currentAction == NULL)
		doAction(_actions.front());

	if (_isMoving == true)
	{
		_movePercent += moveSpeed * dt;
		if (_movePercent >= 1.0)
			onMoveFinished();

		switch (_moveDirection)
		{
		case TILE_DIR_UP:
			drawOffsetY = -_movePercent;
			break;
		case TILE_DIR_DOWN:
			drawOffsetY = _movePercent;
			break;
		case TILE_DIR_LEFT:
			drawOffsetX = -_movePercent;
			break;
		case TILE_DIR_RIGHT:
			drawOffsetX = _movePercent;
			break;
		}
	}
}


// ACTION HANDLERS //
void Tile::addAction(Action* action)
{
	_actions.push_back(action);
}

void Tile::doAction(Action* action)
{
	_currentAction = action;
	switch (action->getActionType())
	{
	case TILE_ACTION_MOVEUP:
		if (startMovingUp() == false)
			onCompletedAction(false);
		break;
	case TILE_ACTION_MOVEDOWN:
		if (startMovingDown() == false)
			onCompletedAction(false);
		break;
	case TILE_ACTION_MOVELEFT:
		if (startMovingLeft() == false)
			onCompletedAction(false);
		break;
	case TILE_ACTION_MOVERIGHT:
		if (startMovingRight() == false)
			onCompletedAction(false);
		break;
	case TILE_ACTION_SETCOLOR:
		doSetColor();
		break;
	default:
		std::cout << "[Tile] Attempted to do an unsupported action of type: " << (int)action->getActionType() << std::endl;
		onCompletedAction(false);
		break;
	}
}

void Tile::onCompletedAction(bool result)
{
	Action* action = _actions.front();
	_actions.remove(action);
	action->result_bool = result;
	action->isCompleted = true;
	_currentAction = NULL;
}


// ACTION METHODS //
Action* Tile::moveUp()
{
	Action* action = new Action(TILE_ACTION_MOVEUP);
	addAction(action);
	return action;
}

Action* Tile::moveDown()
{
	Action* action = new Action(TILE_ACTION_MOVEDOWN);
	addAction(action);
	return action;
}

Action* Tile::moveLeft()
{
	Action* action = new Action(TILE_ACTION_MOVELEFT);
	addAction(action);
	return action;
}

Action* Tile::moveRight()
{
	Action* action = new Action(TILE_ACTION_MOVERIGHT);
	addAction(action);
	return action;
}


Action* Tile::setColor(std::vector<int> colors)
{
	Action* action = new Action(TILE_ACTION_SETCOLOR);
	action->arg_intvector = colors;
	addAction(action);
	return action;
}


// ACTION STARTERS //
bool Tile::startMovingUp()
{
	if (chunk != NULL && _isMoving == false)
	{
		if (y > 0 && chunk->lockTile(x, y - 1, this))
		{
			_lockedX = x;
			_lockedY = y - 1;
			_lockedChunk = chunk;
			_moveDirection = TILE_DIR_UP;
			_isMoving = true;
			return true;
		}
		else if (y == 0 && chunk->hasChunkUp())
		{
			Chunk* nextChunk = chunk->getChunkUp();
			int nextY = nextChunk->getHeight() - 1;
			if (nextChunk->lockTile(x, nextY, this))
			{
				_lockedX = x;
				_lockedY = nextY;
				_lockedChunk = nextChunk;
				_moveDirection = TILE_DIR_UP;
				_isMoving = true;
				return true;
			}
		}
	}
	return false;
}

bool Tile::startMovingDown()
{
	if (chunk != NULL && _isMoving == false)
	{
		if (y < chunk->getHeight() - 1 && chunk->lockTile(x, y + 1, this))
		{
			_lockedX = x;
			_lockedY = y + 1;
			_lockedChunk = chunk;
			_moveDirection = TILE_DIR_DOWN;
			_isMoving = true;
			return true;
		}
		else if (y == chunk->getHeight() - 1 && chunk->hasChunkDown())
		{
			Chunk* nextChunk = chunk->getChunkDown();
			int nextY = 0;
			if (nextChunk->lockTile(x, nextY, this))
			{
				_lockedX = x;
				_lockedY = nextY;
				_lockedChunk = nextChunk;
				_moveDirection = TILE_DIR_DOWN;
				_isMoving = true;
				return true;
			}
		}
	}
	return false;
}

bool Tile::startMovingLeft()
{
	if (chunk != NULL && _isMoving == false)
	{
		if (x > 0 && chunk->lockTile(x - 1, y, this))
		{
			_lockedX = x - 1;
			_lockedY = y;
			_lockedChunk = chunk;
			_moveDirection = TILE_DIR_LEFT;
			_isMoving = true;
			return true;
		}
		else if (x == 0 && chunk->hasChunkLeft())
		{
			Chunk* nextChunk = chunk->getChunkLeft();
			int nextX = nextChunk->getWidth() - 1;
			if (nextChunk->lockTile(nextX, y, this))
			{
				_lockedX = nextX;
				_lockedY = y;
				_lockedChunk = nextChunk;
				_moveDirection = TILE_DIR_LEFT;
				_isMoving = true;
				return true;
			}
		}
	}
	return false;
}

bool Tile::startMovingRight()
{
	if (chunk != NULL && _isMoving == false)
	{
		if (x < chunk->getWidth() - 1 && chunk->lockTile(x + 1, y, this))
		{
			_lockedX = x + 1;
			_lockedY = y;
			_lockedChunk = chunk;
			_moveDirection = TILE_DIR_RIGHT;
			_isMoving = true;
			return true;
		}
		else if (x == chunk->getWidth() - 1 && chunk->hasChunkRight())
		{
			Chunk* nextChunk = chunk->getChunkRight();
			int nextX = 0;
			if (nextChunk->lockTile(nextX, y, this))
			{
				_lockedX = nextX;
				_lockedY = y;
				_lockedChunk = nextChunk;
				_moveDirection = TILE_DIR_RIGHT;
				_isMoving = true;
				return true;
			}
		}
	}
	return false;
}


void Tile::doSetColor()
{
	std::vector<int> colors = _currentAction->arg_intvector;
	size_t count = colors.size();

	if (count > 0 && colors[0] != -1) r = colors[0];
	if (count > 1 && colors[1] != -1) g = colors[1];
	if (count > 2 && colors[2] != -1) b = colors[2];

	onCompletedAction(true);
}


// MOVE FINISHED HANDLERS //
void Tile::onMoveUpFinished()
{
	if (y > 0)
	{
		chunk->unlockTile(x, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		y--;
	}
	else if (y == 0)
	{
		Chunk* nextChunk = _lockedChunk;
		int nextY = _lockedY;
		chunk->removeTileAt(x, y, false);
		nextChunk->unlockTile(x, nextY);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		nextChunk->addTile(x, nextY, this, false);
	}
	//std::cout << "Tile Finished Moving Up X, Y == " << x << ", " << y << std::endl;
}

void Tile::onMoveDownFinished()
{
	if (y < chunk->getHeight() - 1)
	{
		chunk->unlockTile(x, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		y++;
	}
	else if (y == chunk->getHeight() - 1)
	{
		Chunk* nextChunk = _lockedChunk;
		int nextY = _lockedY;
		chunk->removeTileAt(x, y, false);
		nextChunk->unlockTile(x, nextY);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		nextChunk->addTile(x, nextY, this, false);
	}
	//std::cout << "Tile Finished Moving Down X, Y == " << x << ", " << y << std::endl;
}

void Tile::onMoveLeftFinished()
{
	if (x > 0)
	{
		chunk->unlockTile(x, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		x--;
	}
	else if (x == 0)
	{
		Chunk* nextChunk = _lockedChunk;
		int nextX = _lockedX;
		chunk->removeTileAt(x, y, false);
		nextChunk->unlockTile(nextX, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		nextChunk->addTile(nextX, y, this, false);
	}
	//std::cout << "Tile Finished Moving Left X, Y == " << x << ", " << y << std::endl;
}

void Tile::onMoveRightFinished()
{
	if (x < chunk->getWidth() - 1)
	{
		chunk->unlockTile(x, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		x++;
	}
	else if (x == chunk->getWidth() - 1)
	{
		Chunk* nextChunk = _lockedChunk;
		int nextX = _lockedX;
		chunk->removeTileAt(x, y, false);
		nextChunk->unlockTile(nextX, y);
		_lockedChunk = NULL;
		_lockedX = 0;
		_lockedY = 0;
		nextChunk->addTile(nextX, y, this, false);
	}
	//std::cout << "Tile Finished Moving Right X, Y == " << x << ", " << y << std::endl;
}

void Tile::onMoveFinished()
{
	_movePercent = 0;

	switch (_moveDirection)
	{
	case TILE_DIR_UP:
		onMoveUpFinished();
		break;
	case TILE_DIR_DOWN:
		onMoveDownFinished();
		break;
	case TILE_DIR_LEFT:
		onMoveLeftFinished();
		break;
	case TILE_DIR_RIGHT:
		onMoveRightFinished();
		break;
	}
	drawOffsetX = 0;
	drawOffsetY = 0;

	_isMoving = false;
	onCompletedAction(true);
}


uint16_t Tile::getID()
{
	return _id;
}

bool Tile::getIsMoving()
{
	return _isMoving;
}

int Tile::getMoveDirection()
{
	return _moveDirection;
}

Interpreter* Tile::getInterpreter()
{
	return _interpreter;
}

Terminal* Tile::getTerminal()
{
	return _terminal;
}
