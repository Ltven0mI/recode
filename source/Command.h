#pragma once

#include "Terminal.h"
#include "Interpreter.h"

#include <iostream>
#include <string>
#include <vector>
#include <map>

#define ERROR_COMMAND_FAILED 1
#define ERROR_COMMAND_MISSINGARGS 2

struct Argument
{
    std::string name;
    std::string info;

    Argument(std::string name, std::string info = "")
    {
        this->name = name;
        this->info = info;
    }
};

class Terminal;
class Interpreter;
class Command
{
public:
    Command(std::string title, std::vector<Argument> args, uint16_t requiredArgCount, bool (*function)(std::vector<std::string>, Terminal*, Interpreter*));
    ~Command();

    int run(std::vector<std::string> args, Terminal* term, Interpreter* interpreter);

    std::string getTitle();
    std::vector<Argument> getArgs();
    uint16_t getRequiredArgCount();

    std::string getHelpString();
private:
    std::string _title;
    std::vector<Argument> _args;
    uint16_t _requiredArgCount;
    bool (*_function)(std::vector<std::string>, Terminal*, Interpreter*);

    std::string _helpString;
};
