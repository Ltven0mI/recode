#pragma once

#include <string>
#include <vector>

// Tile Actions //
#define TILE_ACTION_MOVEUP 1
#define TILE_ACTION_MOVEDOWN 2
#define TILE_ACTION_MOVELEFT 3
#define TILE_ACTION_MOVERIGHT 4
#define TILE_ACTION_SETCOLOR 5

// Terminal Actions //
#define TERM_ACTION_READ 20
#define TERM_ACTION_WRITE 21
#define TERM_ACTION_CLEAR 22
#define TERM_ACTION_PROGRAMEND 23

class Action
{
public:
	bool isCompleted;
	bool result_bool;
	std::string result_string;

	std::string arg_string;
	std::vector<int> arg_intvector;

	Action(int actionType);
	~Action();

	int getActionType();
private:
	int _actionType;
};
