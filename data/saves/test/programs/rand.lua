while true do
  local rand = math.random(4)
  if rand == 1 then
    tile.moveUp()
  elseif rand == 2 then
    tile.moveDown()
  elseif rand == 3 then
    tile.moveLeft()
  elseif rand == 4 then
    tile.moveRight()
  else
    shell.sleep(250)
  end
end
