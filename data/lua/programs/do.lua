local args = {...}

local handles = {
  ["up"] = tile.moveUp,
  ["down"] = tile.moveDown,
  ["left"] = tile.moveLeft,
  ["right"] = tile.moveRight
}

local function doHandle(handle, num)
  for i=1, num do
    handles[handle]()
  end
end

for k, v in pairs(args) do
  if handles[v] ~= nil then
    if tonumber(args[k+1]) ~= nil then
      doHandle(v, tonumber(args[k+1]))
    else
      doHandle(v, 1)
    end
  end
end
