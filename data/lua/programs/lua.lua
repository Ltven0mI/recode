while true do
  write("lua> ")
  local input = read()
  local funcResult = load(input)
  local argResult = {pcall(funcResult)}
  for k, v in pairs(argResult) do
    write(tostring(v).." ")
  end
  write("\n")
end
